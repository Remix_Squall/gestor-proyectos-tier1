<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.lrm.gestor.model.impl.ProyectoImpl" %>
<%@page import="java.util.List"%>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil" %>
<%@page import="com.lrm.gestor.service.TareaLocalServiceUtil" %>
<%@page import="com.lrm.gestor.model.Proyecto" %>
<%@page import="com.lrm.gestor.model.Tarea" %>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%
	ResultRow currentRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Proyecto proyecto = (Proyecto) currentRow.getObject();
	List proyectoTareasList = TareaLocalServiceUtil.getProyectoTareas(proyecto.getIdProyecto());
	for(int i = 0; i<proyectoTareasList.size(); i++){
		Object curTarea = proyectoTareasList.get(i);
		JSONObject jobj = JSONFactoryUtil.createJSONObject(curTarea.toString());
		out.println(jobj.getString("tarea"));
		out.println("<br/>");
	}
%>
