<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.lrm.gestor.service.persistence.ProyectoUtil" %>
<%@page import="com.lrm.gestor.service.persistence.TareaUtil" %>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.List"%>
<%@page import="com.lrm.gestor.model.Tarea" %>
<%@page import="com.lrm.gestor.service.TareaLocalServiceUtil" %>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil" %>
<%@page import="com.lrm.gestor.model.Proyecto" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(),
						"proyecto-edit-success")%>'>
	<liferay-ui:success key="proyecto-edit-success"
		message="El proyecto ha sido editado con �xito." />
</c:if>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(),
						"proyecto-edit-failed")%>'>
	<liferay-ui:error key="proyecto-edit-failed"
		message="Fallo al editar el proyecto." />
</c:if>
<%
PortletURL iteratorURL = renderResponse.createRenderURL();
iteratorURL.setParameter("mvcPath", "/html/jsp/display_project_tasks.jsp");
%>
<portlet:actionURL var="deleteProjectURL">
<portlet:param name="action" value="deleteProyecto"/>
</portlet:actionURL>
<liferay-ui:search-container delta="10" emptyResultsMessage="No hay ning�n proyecto"  deltaConfigurable="true" iteratorURL="<%=iteratorURL%>">
		<liferay-ui:search-container-results >
		<%
			results=ProyectoLocalServiceUtil.getProyectos(searchContainer.getStart(), searchContainer.getEnd());
			total = ProyectoLocalServiceUtil.getProyectosCount();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="Proyecto" keyProperty="idProyecto" modelVar="proyecto" rowVar="curRow" escapedModel="<%= true %>">
		<liferay-ui:search-container-column-text name="C�digo Proyecto" property="codigo" />
		<liferay-ui:search-container-column-text name="Descripci�n Proyecto" property="descripcion" />
		<liferay-ui:search-container-column-text name="Fecha Inicio" property="fechaInicio" />
		<liferay-ui:search-container-column-text name="Coste" property="coste" />
	<liferay-ui:search-container-column-jsp path="/html/jsp/show_tasks.jsp" align="right" name="Tareas"/>
	<liferay-ui:search-container-column-jsp path="/html/jsp/actions_project.jsp" align="right" name="" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>


