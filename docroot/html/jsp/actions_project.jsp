<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.lrm.gestor.model.Proyecto"%>
<%@page import="com.lrm.gestor.model.impl.ProyectoImpl"%>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>

<%
	ResultRow currentRow = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Proyecto proyecto = (Proyecto) currentRow.getObject();
%>
<liferay-ui:icon-menu>
	<portlet:renderURL var="editURL">
		<portlet:param name="idProyecto" value="<%=String.valueOf(proyecto.getIdProyecto()) %>"/>
		<portlet:param name="mvcPath" value="/html/jsp/edit_project.jsp"/>
	</portlet:renderURL>
	<liferay-ui:icon image="edit" message="Editar" url="<%=editURL %>" />
	<portlet:actionURL name="deleteProyecto" var="deleteURL">
		<portlet:param name="idProyecto" value="<%=String.valueOf(proyecto.getIdProyecto()) %>"/>
	</portlet:actionURL>
	<liferay-ui:icon-delete url="<%=deleteURL %>"></liferay-ui:icon-delete>
</liferay-ui:icon-menu>