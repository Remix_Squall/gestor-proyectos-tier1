/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.lrm.gestor.model.Proyecto;

import java.util.List;

/**
 * The persistence utility for the proyecto service. This utility wraps {@link ProyectoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luis Romero Moreno
 * @see ProyectoPersistence
 * @see ProyectoPersistenceImpl
 * @generated
 */
public class ProyectoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Proyecto proyecto) {
		getPersistence().clearCache(proyecto);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Proyecto> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Proyecto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Proyecto> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Proyecto update(Proyecto proyecto, boolean merge)
		throws SystemException {
		return getPersistence().update(proyecto, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Proyecto update(Proyecto proyecto, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(proyecto, merge, serviceContext);
	}

	/**
	* Caches the proyecto in the entity cache if it is enabled.
	*
	* @param proyecto the proyecto
	*/
	public static void cacheResult(com.lrm.gestor.model.Proyecto proyecto) {
		getPersistence().cacheResult(proyecto);
	}

	/**
	* Caches the proyectos in the entity cache if it is enabled.
	*
	* @param proyectos the proyectos
	*/
	public static void cacheResult(
		java.util.List<com.lrm.gestor.model.Proyecto> proyectos) {
		getPersistence().cacheResult(proyectos);
	}

	/**
	* Creates a new proyecto with the primary key. Does not add the proyecto to the database.
	*
	* @param idProyecto the primary key for the new proyecto
	* @return the new proyecto
	*/
	public static com.lrm.gestor.model.Proyecto create(long idProyecto) {
		return getPersistence().create(idProyecto);
	}

	/**
	* Removes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto that was removed
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto remove(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().remove(idProyecto);
	}

	public static com.lrm.gestor.model.Proyecto updateImpl(
		com.lrm.gestor.model.Proyecto proyecto, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(proyecto, merge);
	}

	/**
	* Returns the proyecto with the primary key or throws a {@link com.lrm.gestor.NoSuchProyectoException} if it could not be found.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findByPrimaryKey(
		long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().findByPrimaryKey(idProyecto);
	}

	/**
	* Returns the proyecto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto, or <code>null</code> if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchByPrimaryKey(
		long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idProyecto);
	}

	/**
	* Returns all the proyectos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyCodigo(codigo);
	}

	/**
	* Returns a range of all the proyectos where codigo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param codigo the codigo
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyCodigo(codigo, start, end);
	}

	/**
	* Returns an ordered range of all the proyectos where codigo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param codigo the codigo
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybyCodigo(codigo, start, end, orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyCodigo_First(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().findBybyCodigo_First(codigo, orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyCodigo_First(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyCodigo_First(codigo, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyCodigo_Last(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().findBybyCodigo_Last(codigo, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyCodigo_Last(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyCodigo_Last(codigo, orderByComparator);
	}

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where codigo = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto[] findBybyCodigo_PrevAndNext(
		long idProyecto, java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence()
				   .findBybyCodigo_PrevAndNext(idProyecto, codigo,
			orderByComparator);
	}

	/**
	* Returns all the proyectos where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyFechaInicio(fechaInicio);
	}

	/**
	* Returns a range of all the proyectos where fechaInicio = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fechaInicio the fecha inicio
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyFechaInicio(fechaInicio, start, end);
	}

	/**
	* Returns an ordered range of all the proyectos where fechaInicio = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fechaInicio the fecha inicio
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybyFechaInicio(fechaInicio, start, end,
			orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyFechaInicio_First(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence()
				   .findBybyFechaInicio_First(fechaInicio, orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyFechaInicio_First(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybyFechaInicio_First(fechaInicio, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyFechaInicio_Last(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence()
				   .findBybyFechaInicio_Last(fechaInicio, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyFechaInicio_Last(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybyFechaInicio_Last(fechaInicio, orderByComparator);
	}

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto[] findBybyFechaInicio_PrevAndNext(
		long idProyecto, java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence()
				   .findBybyFechaInicio_PrevAndNext(idProyecto, fechaInicio,
			orderByComparator);
	}

	/**
	* Returns all the proyectos where coste = &#63;.
	*
	* @param coste the coste
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyCoste(coste);
	}

	/**
	* Returns a range of all the proyectos where coste = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param coste the coste
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyCoste(coste, start, end);
	}

	/**
	* Returns an ordered range of all the proyectos where coste = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param coste the coste
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybyCoste(coste, start, end, orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyCoste_First(
		double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().findBybyCoste_First(coste, orderByComparator);
	}

	/**
	* Returns the first proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyCoste_First(
		double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyCoste_First(coste, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto findBybyCoste_Last(
		double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence().findBybyCoste_Last(coste, orderByComparator);
	}

	/**
	* Returns the last proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto fetchBybyCoste_Last(
		double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyCoste_Last(coste, orderByComparator);
	}

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where coste = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto[] findBybyCoste_PrevAndNext(
		long idProyecto, double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException {
		return getPersistence()
				   .findBybyCoste_PrevAndNext(idProyecto, coste,
			orderByComparator);
	}

	/**
	* Returns all the proyectos.
	*
	* @return the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the proyectos where codigo = &#63; from the database.
	*
	* @param codigo the codigo
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybyCodigo(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybyCodigo(codigo);
	}

	/**
	* Removes all the proyectos where fechaInicio = &#63; from the database.
	*
	* @param fechaInicio the fecha inicio
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybyFechaInicio(java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybyFechaInicio(fechaInicio);
	}

	/**
	* Removes all the proyectos where coste = &#63; from the database.
	*
	* @param coste the coste
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybyCoste(double coste)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybyCoste(coste);
	}

	/**
	* Removes all the proyectos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of proyectos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybyCodigo(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybyCodigo(codigo);
	}

	/**
	* Returns the number of proyectos where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybyFechaInicio(java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybyFechaInicio(fechaInicio);
	}

	/**
	* Returns the number of proyectos where coste = &#63;.
	*
	* @param coste the coste
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybyCoste(double coste)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybyCoste(coste);
	}

	/**
	* Returns the number of proyectos.
	*
	* @return the number of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the tareas associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @return the tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> getTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getTareas(pk);
	}

	/**
	* Returns a range of all the tareas associated with the proyecto.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the proyecto
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> getTareas(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getTareas(pk, start, end);
	}

	/**
	* Returns an ordered range of all the tareas associated with the proyecto.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the proyecto
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> getTareas(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getTareas(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of tareas associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @return the number of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static int getTareasSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getTareasSize(pk);
	}

	/**
	* Returns <code>true</code> if the tarea is associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @return <code>true</code> if the tarea is associated with the proyecto; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsTarea(pk, tareaPK);
	}

	/**
	* Returns <code>true</code> if the proyecto has any tareas associated with it.
	*
	* @param pk the primary key of the proyecto to check for associations with tareas
	* @return <code>true</code> if the proyecto has any tareas associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsTareas(pk);
	}

	/**
	* Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void addTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addTarea(pk, tareaPK);
	}

	/**
	* Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tarea the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void addTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addTarea(pk, tarea);
	}

	/**
	* Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addTareas(pk, tareaPKs);
	}

	/**
	* Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addTareas(pk, tareas);
	}

	/**
	* Clears all associations between the proyecto and its tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto to clear the associated tareas from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearTareas(pk);
	}

	/**
	* Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void removeTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeTarea(pk, tareaPK);
	}

	/**
	* Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tarea the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void removeTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeTarea(pk, tarea);
	}

	/**
	* Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas
	* @throws SystemException if a system exception occurred
	*/
	public static void removeTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeTareas(pk, tareaPKs);
	}

	/**
	* Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas
	* @throws SystemException if a system exception occurred
	*/
	public static void removeTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeTareas(pk, tareas);
	}

	/**
	* Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas to be associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void setTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setTareas(pk, tareaPKs);
	}

	/**
	* Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas to be associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void setTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setTareas(pk, tareas);
	}

	public static ProyectoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ProyectoPersistence)PortletBeanLocatorUtil.locate(com.lrm.gestor.service.ClpSerializer.getServletContextName(),
					ProyectoPersistence.class.getName());

			ReferenceRegistry.registerReference(ProyectoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ProyectoPersistence persistence) {
	}

	private static ProyectoPersistence _persistence;
}