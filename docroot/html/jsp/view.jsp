<%@ include file="init.jsp"%>

<portlet:renderURL var="addProjectURL">
<portlet:param name="mvcPath" value="/html/jsp/altaProyecto.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="addTaskURL">
<portlet:param name="mvcPath" value="/html/jsp/altaTarea.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="mapProjectsToTaskURL">
<portlet:param name="mvcPath" value="/html/jsp/map_projects_to_task.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="mapTasksToProject">
<portlet:param name="mvcPath" value="/html/jsp/map_tasks_to_project.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="showProjectTasks">
<portlet:param name="mvcPath" value="/html/jsp/display_project_tasks.jsp"/>
</portlet:renderURL>
<h1>Gestor de Proyectos</h1>
<aui:a href="<%=addProjectURL%>" label="Alta Proyecto"></aui:a><br/>
<aui:a href="<%=addTaskURL%>" label="Alta Tarea"></aui:a><br/>
<aui:a href="<%=mapProjectsToTaskURL%>" label="Asignar Proyectos a Tarea"></aui:a><br/>
<aui:a href="<%=mapTasksToProject%>" label="Asignar Tareas a Proyecto"></aui:a><br/>
<aui:a href="<%=showProjectTasks%>" label="Mostrar Proyectos con Tareas"></aui:a><br/>