/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.lrm.gestor.service.ClpSerializer;
import com.lrm.gestor.service.TareaLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luis Romero Moreno
 */
public class TareaClp extends BaseModelImpl<Tarea> implements Tarea {
	public TareaClp() {
	}

	public Class<?> getModelClass() {
		return Tarea.class;
	}

	public String getModelClassName() {
		return Tarea.class.getName();
	}

	public long getPrimaryKey() {
		return _idTarea;
	}

	public void setPrimaryKey(long primaryKey) {
		setIdTarea(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_idTarea);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idTarea", getIdTarea());
		attributes.put("tarea", getTarea());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idTarea = (Long)attributes.get("idTarea");

		if (idTarea != null) {
			setIdTarea(idTarea);
		}

		String tarea = (String)attributes.get("tarea");

		if (tarea != null) {
			setTarea(tarea);
		}
	}

	public long getIdTarea() {
		return _idTarea;
	}

	public void setIdTarea(long idTarea) {
		_idTarea = idTarea;

		if (_tareaRemoteModel != null) {
			try {
				Class<?> clazz = _tareaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdTarea", long.class);

				method.invoke(_tareaRemoteModel, idTarea);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getTarea() {
		return _tarea;
	}

	public void setTarea(String tarea) {
		_tarea = tarea;

		if (_tareaRemoteModel != null) {
			try {
				Class<?> clazz = _tareaRemoteModel.getClass();

				Method method = clazz.getMethod("setTarea", String.class);

				method.invoke(_tareaRemoteModel, tarea);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTareaRemoteModel() {
		return _tareaRemoteModel;
	}

	public void setTareaRemoteModel(BaseModel<?> tareaRemoteModel) {
		_tareaRemoteModel = tareaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _tareaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_tareaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			TareaLocalServiceUtil.addTarea(this);
		}
		else {
			TareaLocalServiceUtil.updateTarea(this);
		}
	}

	@Override
	public Tarea toEscapedModel() {
		return (Tarea)ProxyUtil.newProxyInstance(Tarea.class.getClassLoader(),
			new Class[] { Tarea.class }, new AutoEscapeBeanHandler(this));
	}

	public Tarea toUnescapedModel() {
		return this;
	}

	@Override
	public Object clone() {
		TareaClp clone = new TareaClp();

		clone.setIdTarea(getIdTarea());
		clone.setTarea(getTarea());

		return clone;
	}

	public int compareTo(Tarea tarea) {
		int value = 0;

		if (getIdTarea() < tarea.getIdTarea()) {
			value = -1;
		}
		else if (getIdTarea() > tarea.getIdTarea()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TareaClp)) {
			return false;
		}

		TareaClp tarea = (TareaClp)obj;

		long primaryKey = tarea.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{idTarea=");
		sb.append(getIdTarea());
		sb.append(", tarea=");
		sb.append(getTarea());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.lrm.gestor.model.Tarea");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idTarea</column-name><column-value><![CDATA[");
		sb.append(getIdTarea());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tarea</column-name><column-value><![CDATA[");
		sb.append(getTarea());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idTarea;
	private String _tarea;
	private BaseModel<?> _tareaRemoteModel;
}