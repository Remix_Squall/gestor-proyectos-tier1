/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ProyectoLocalService}.
 * </p>
 *
 * @author    Luis Romero Moreno
 * @see       ProyectoLocalService
 * @generated
 */
public class ProyectoLocalServiceWrapper implements ProyectoLocalService,
	ServiceWrapper<ProyectoLocalService> {
	public ProyectoLocalServiceWrapper(
		ProyectoLocalService proyectoLocalService) {
		_proyectoLocalService = proyectoLocalService;
	}

	/**
	* Adds the proyecto to the database. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto addProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.addProyecto(proyecto);
	}

	/**
	* Creates a new proyecto with the primary key. Does not add the proyecto to the database.
	*
	* @param idProyecto the primary key for the new proyecto
	* @return the new proyecto
	*/
	public com.lrm.gestor.model.Proyecto createProyecto(long idProyecto) {
		return _proyectoLocalService.createProyecto(idProyecto);
	}

	/**
	* Deletes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto that was removed
	* @throws PortalException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto deleteProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.deleteProyecto(idProyecto);
	}

	/**
	* Deletes the proyecto from the database. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto deleteProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.deleteProyecto(proyecto);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _proyectoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.lrm.gestor.model.Proyecto fetchProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.fetchProyecto(idProyecto);
	}

	/**
	* Returns the proyecto with the primary key.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto
	* @throws PortalException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto getProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getProyecto(idProyecto);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> getProyectos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getProyectos(start, end);
	}

	/**
	* Returns the number of proyectos.
	*
	* @return the number of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public int getProyectosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getProyectosCount();
	}

	/**
	* Updates the proyecto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto updateProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.updateProyecto(proyecto);
	}

	/**
	* Updates the proyecto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @param merge whether to merge the proyecto with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the proyecto that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto updateProyecto(
		com.lrm.gestor.model.Proyecto proyecto, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.updateProyecto(proyecto, merge);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.addTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addTareaProyecto(long idTarea,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.addTareaProyecto(idTarea, proyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.addTareaProyectos(idTarea, idProyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addTareaProyectos(long idTarea,
		java.util.List<com.lrm.gestor.model.Proyecto> Proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.addTareaProyectos(idTarea, Proyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void clearTareaProyectos(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.clearTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.deleteTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteTareaProyecto(long idTarea,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.deleteTareaProyecto(idTarea, proyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.deleteTareaProyectos(idTarea, idProyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteTareaProyectos(long idTarea,
		java.util.List<com.lrm.gestor.model.Proyecto> Proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.deleteTareaProyectos(idTarea, Proyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getTareaProyectos(idTarea, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getTareaProyectos(idTarea, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public int getTareaProyectosCount(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.getTareaProyectosCount(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public boolean hasTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.hasTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public boolean hasTareaProyectos(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _proyectoLocalService.hasTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void setTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyectoLocalService.setTareaProyectos(idTarea, idProyectos);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _proyectoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_proyectoLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _proyectoLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ProyectoLocalService getWrappedProyectoLocalService() {
		return _proyectoLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedProyectoLocalService(
		ProyectoLocalService proyectoLocalService) {
		_proyectoLocalService = proyectoLocalService;
	}

	public ProyectoLocalService getWrappedService() {
		return _proyectoLocalService;
	}

	public void setWrappedService(ProyectoLocalService proyectoLocalService) {
		_proyectoLocalService = proyectoLocalService;
	}

	private ProyectoLocalService _proyectoLocalService;
}