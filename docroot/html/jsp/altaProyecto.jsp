<%@page import="com.lrm.gestor.model.Proyecto" %>
<%@page import="com.lrm.gestor.model.impl.ProyectoImpl"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>
<%
Proyecto proyecto = new ProyectoImpl();
%>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(),
						"project-add-success")%>'>
	<liferay-ui:success key="project-add-success"
		message="El proyecto ha sido a�adido con �xito." />
</c:if>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(), "project-add-failed")%>'>
	<liferay-ui:error key="project-add-failed"
		message="Fallo al a�adir el proyecto." />
</c:if>
<portlet:actionURL var="addProjectActionURL" name="addProject">
</portlet:actionURL>
<h1>Alta Proyecto</h1>
<aui:model-context bean="<%=proyecto%>" model="<%=Proyecto.class%>" />
<aui:form action="<%=addProjectActionURL%>">
	<aui:input name="codigo" label="C�digo Proyecto" />
	<aui:input name="descripcion" label="Descripci�n proyecto" />
	<aui:input name="fechaInicio" label="Fecha Inicio" />
	<aui:input name="coste" label="Coste" />
	<aui:button-row>
		<aui:button value="Alta Proyecto" type="submit" />
	</aui:button-row>
</aui:form>