<%@page import="com.lrm.gestor.model.Proyecto" %>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil" %>
<%@page import="com.lrm.gestor.model.Tarea" %>
<%@page import="java.util.List"%>
<%@page import="com.lrm.gestor.service.TareaLocalServiceUtil" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>
<c:if test='<%=SessionMessages.contains(renderRequest.getPortletSession(),"tareas-add-success")%>'>
<liferay-ui:success key="tareas-add-success" message="Las tareas fueron asignadas al proyecto con �xito." />
</c:if>
<c:if test='<%=SessionMessages.contains(renderRequest.getPortletSession(),"tareas-add-failed")%>'>
<liferay-ui:error key="tareas-add-failed" message="Fallo al asignar tareas al proyecto." />
</c:if>
<portlet:actionURL  var="mapTareasToProyectoActionURL" name="mapTareas">
</portlet:actionURL>
<h1>Asignar proyectos a una tarea</h1>
<aui:form action="<%= mapTareasToProyectoActionURL %>" method="post" name="tareasMapForm" id="tareasMapForm">
<aui:select name="proyecto" label="C�digo Proyecto">
<%
List<Proyecto> proyectoList=ProyectoLocalServiceUtil.getProyectos(0,ProyectoLocalServiceUtil.getProyectosCount());
for(Proyecto proyecto:proyectoList){%>
<aui:option value="<%=proyecto.getIdProyecto()%>" label="<%=proyecto.getCodigo()%>"></aui:option>
<%}%>

</aui:select>
<%
List<Tarea> tareaList=TareaLocalServiceUtil.getTareas(0,TareaLocalServiceUtil.getTareasCount());
for(Tarea tarea:tareaList){%>
<aui:input name="tareas" label="<%=tarea.getTarea()%>" value="<%=tarea.getIdTarea()%>" type="checkbox"></aui:input>
<%}%>
<aui:input name="selectedTareasHidden" id="selectedTareasHidden" type="hidden" value="" />
<aui:button-row>
<aui:button type="button" name="mapTareas" id="mapTareas" value="Asignar tareas"/>
</aui:button-row>
<aui:script>
AUI().use('aui-base', function(A) {
A.one('#<portlet:namespace/>mapTareas').on('click', function(event){ 
 //alert("==");
 var selectedTareas="";
 A.one('#<portlet:namespace/>tareasMapForm').all('input[type=checkbox]').each(function() {
    if(this.get('checked')){
 	selectedTareas=this.get('value')+";"+selectedTareas;
	}
 });
 A.one('#<portlet:namespace />selectedTareasHidden').set("value",selectedTareas.trim());
 document.<portlet:namespace />tareasMapForm.submit();
 });
});
</aui:script>
</aui:form>