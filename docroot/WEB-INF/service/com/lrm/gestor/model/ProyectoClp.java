/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.lrm.gestor.service.ClpSerializer;
import com.lrm.gestor.service.ProyectoLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Luis Romero Moreno
 */
public class ProyectoClp extends BaseModelImpl<Proyecto> implements Proyecto {
	public ProyectoClp() {
	}

	public Class<?> getModelClass() {
		return Proyecto.class;
	}

	public String getModelClassName() {
		return Proyecto.class.getName();
	}

	public long getPrimaryKey() {
		return _idProyecto;
	}

	public void setPrimaryKey(long primaryKey) {
		setIdProyecto(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_idProyecto);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idProyecto", getIdProyecto());
		attributes.put("codigo", getCodigo());
		attributes.put("descripcion", getDescripcion());
		attributes.put("fechaInicio", getFechaInicio());
		attributes.put("coste", getCoste());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long idProyecto = (Long)attributes.get("idProyecto");

		if (idProyecto != null) {
			setIdProyecto(idProyecto);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Date fechaInicio = (Date)attributes.get("fechaInicio");

		if (fechaInicio != null) {
			setFechaInicio(fechaInicio);
		}

		Double coste = (Double)attributes.get("coste");

		if (coste != null) {
			setCoste(coste);
		}
	}

	public long getIdProyecto() {
		return _idProyecto;
	}

	public void setIdProyecto(long idProyecto) {
		_idProyecto = idProyecto;

		if (_proyectoRemoteModel != null) {
			try {
				Class<?> clazz = _proyectoRemoteModel.getClass();

				Method method = clazz.getMethod("setIdProyecto", long.class);

				method.invoke(_proyectoRemoteModel, idProyecto);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;

		if (_proyectoRemoteModel != null) {
			try {
				Class<?> clazz = _proyectoRemoteModel.getClass();

				Method method = clazz.getMethod("setCodigo", String.class);

				method.invoke(_proyectoRemoteModel, codigo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;

		if (_proyectoRemoteModel != null) {
			try {
				Class<?> clazz = _proyectoRemoteModel.getClass();

				Method method = clazz.getMethod("setDescripcion", String.class);

				method.invoke(_proyectoRemoteModel, descripcion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public Date getFechaInicio() {
		return _fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		_fechaInicio = fechaInicio;

		if (_proyectoRemoteModel != null) {
			try {
				Class<?> clazz = _proyectoRemoteModel.getClass();

				Method method = clazz.getMethod("setFechaInicio", Date.class);

				method.invoke(_proyectoRemoteModel, fechaInicio);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public double getCoste() {
		return _coste;
	}

	public void setCoste(double coste) {
		_coste = coste;

		if (_proyectoRemoteModel != null) {
			try {
				Class<?> clazz = _proyectoRemoteModel.getClass();

				Method method = clazz.getMethod("setCoste", double.class);

				method.invoke(_proyectoRemoteModel, coste);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getProyectoRemoteModel() {
		return _proyectoRemoteModel;
	}

	public void setProyectoRemoteModel(BaseModel<?> proyectoRemoteModel) {
		_proyectoRemoteModel = proyectoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _proyectoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_proyectoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ProyectoLocalServiceUtil.addProyecto(this);
		}
		else {
			ProyectoLocalServiceUtil.updateProyecto(this);
		}
	}

	@Override
	public Proyecto toEscapedModel() {
		return (Proyecto)ProxyUtil.newProxyInstance(Proyecto.class.getClassLoader(),
			new Class[] { Proyecto.class }, new AutoEscapeBeanHandler(this));
	}

	public Proyecto toUnescapedModel() {
		return this;
	}

	@Override
	public Object clone() {
		ProyectoClp clone = new ProyectoClp();

		clone.setIdProyecto(getIdProyecto());
		clone.setCodigo(getCodigo());
		clone.setDescripcion(getDescripcion());
		clone.setFechaInicio(getFechaInicio());
		clone.setCoste(getCoste());

		return clone;
	}

	public int compareTo(Proyecto proyecto) {
		int value = 0;

		value = getCodigo().compareTo(proyecto.getCodigo());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProyectoClp)) {
			return false;
		}

		ProyectoClp proyecto = (ProyectoClp)obj;

		long primaryKey = proyecto.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idProyecto=");
		sb.append(getIdProyecto());
		sb.append(", codigo=");
		sb.append(getCodigo());
		sb.append(", descripcion=");
		sb.append(getDescripcion());
		sb.append(", fechaInicio=");
		sb.append(getFechaInicio());
		sb.append(", coste=");
		sb.append(getCoste());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.lrm.gestor.model.Proyecto");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>idProyecto</column-name><column-value><![CDATA[");
		sb.append(getIdProyecto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>codigo</column-name><column-value><![CDATA[");
		sb.append(getCodigo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>descripcion</column-name><column-value><![CDATA[");
		sb.append(getDescripcion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fechaInicio</column-name><column-value><![CDATA[");
		sb.append(getFechaInicio());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>coste</column-name><column-value><![CDATA[");
		sb.append(getCoste());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _idProyecto;
	private String _codigo;
	private String _descripcion;
	private Date _fechaInicio;
	private double _coste;
	private BaseModel<?> _proyectoRemoteModel;
}