/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.lrm.gestor.model.Proyecto;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Proyecto in entity cache.
 *
 * @author Luis Romero Moreno
 * @see Proyecto
 * @generated
 */
public class ProyectoCacheModel implements CacheModel<Proyecto>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{idProyecto=");
		sb.append(idProyecto);
		sb.append(", codigo=");
		sb.append(codigo);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", fechaInicio=");
		sb.append(fechaInicio);
		sb.append(", coste=");
		sb.append(coste);
		sb.append("}");

		return sb.toString();
	}

	public Proyecto toEntityModel() {
		ProyectoImpl proyectoImpl = new ProyectoImpl();

		proyectoImpl.setIdProyecto(idProyecto);

		if (codigo == null) {
			proyectoImpl.setCodigo(StringPool.BLANK);
		}
		else {
			proyectoImpl.setCodigo(codigo);
		}

		if (descripcion == null) {
			proyectoImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			proyectoImpl.setDescripcion(descripcion);
		}

		if (fechaInicio == Long.MIN_VALUE) {
			proyectoImpl.setFechaInicio(null);
		}
		else {
			proyectoImpl.setFechaInicio(new Date(fechaInicio));
		}

		proyectoImpl.setCoste(coste);

		proyectoImpl.resetOriginalValues();

		return proyectoImpl;
	}

	public long idProyecto;
	public String codigo;
	public String descripcion;
	public long fechaInicio;
	public double coste;
}