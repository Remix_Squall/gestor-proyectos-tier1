<%@page import="com.lrm.gestor.model.Proyecto" %>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil" %>
<%@page import="com.lrm.gestor.model.Tarea" %>
<%@page import="java.util.List"%>
<%@page import="com.lrm.gestor.service.TareaLocalServiceUtil" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>
<c:if test='<%=SessionMessages.contains(renderRequest.getPortletSession(),"proyectos-add-success")%>'>
<liferay-ui:success key="proyectos-add-success" message="Los proyectos fueron asignados a la tarea con �xito." />
</c:if>
<c:if test='<%=SessionMessages.contains(renderRequest.getPortletSession(),"proyectos-add-failed")%>'>
<liferay-ui:error key="proyectos-add-failed" message="Fallo al asignar proyectos a la tarea." />
</c:if>
<portlet:actionURL  var="mapProyectosToTareaActionURL" name="mapProyectos">
</portlet:actionURL>
<h1>Asignar proyectos a una tarea</h1>
<aui:form action="<%= mapProyectosToTareaActionURL %>" method="post" name="proyectosMapForm" id="proyectosMapForm">
<aui:select name="tarea" label="Nombre Tarea">
<%
List<Tarea> tareaList=TareaLocalServiceUtil.getTareas(0,TareaLocalServiceUtil.getTareasCount());
for(Tarea tarea:tareaList){%>
<aui:option value="<%=tarea.getIdTarea()%>" label="<%=tarea.getTarea()%>"></aui:option>
<%}%>

</aui:select>
<%
List<Proyecto> proyectoList=ProyectoLocalServiceUtil.getProyectos(0,ProyectoLocalServiceUtil.getProyectosCount());
for(Proyecto proyecto:proyectoList){%>
<aui:input name="proyectos" label="<%=proyecto.getCodigo()%>" value="<%=proyecto.getIdProyecto()%>" type="checkbox"></aui:input>
<%}%>
<aui:input name="selectedProyectosHidden" id="selectedProyectosHidden" type="hidden" value="" />
<aui:button-row>
<aui:button type="button" name="mapProyectos" id="mapProyectos" value="Asignar proyectos"/>
</aui:button-row>
<aui:script>
AUI().use('aui-base', function(A) {
A.one('#<portlet:namespace/>mapProyectos').on('click', function(event){ 
 //alert("==");
 var selectedProyectos="";
 A.one('#<portlet:namespace/>proyectosMapForm').all('input[type=checkbox]').each(function() {
    if(this.get('checked')){
 	selectedProyectos=this.get('value')+";"+selectedProyectos;
	}
 });
 A.one('#<portlet:namespace />selectedProyectosHidden').set("value",selectedProyectos.trim());
 document.<portlet:namespace />proyectosMapForm.submit();
 });
});
</aui:script>
</aui:form>