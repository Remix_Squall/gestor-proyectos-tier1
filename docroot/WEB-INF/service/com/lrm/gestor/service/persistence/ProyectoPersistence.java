/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.lrm.gestor.model.Proyecto;

/**
 * The persistence interface for the proyecto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luis Romero Moreno
 * @see ProyectoPersistenceImpl
 * @see ProyectoUtil
 * @generated
 */
public interface ProyectoPersistence extends BasePersistence<Proyecto> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProyectoUtil} to access the proyecto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the proyecto in the entity cache if it is enabled.
	*
	* @param proyecto the proyecto
	*/
	public void cacheResult(com.lrm.gestor.model.Proyecto proyecto);

	/**
	* Caches the proyectos in the entity cache if it is enabled.
	*
	* @param proyectos the proyectos
	*/
	public void cacheResult(
		java.util.List<com.lrm.gestor.model.Proyecto> proyectos);

	/**
	* Creates a new proyecto with the primary key. Does not add the proyecto to the database.
	*
	* @param idProyecto the primary key for the new proyecto
	* @return the new proyecto
	*/
	public com.lrm.gestor.model.Proyecto create(long idProyecto);

	/**
	* Removes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto that was removed
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto remove(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	public com.lrm.gestor.model.Proyecto updateImpl(
		com.lrm.gestor.model.Proyecto proyecto, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the proyecto with the primary key or throws a {@link com.lrm.gestor.NoSuchProyectoException} if it could not be found.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findByPrimaryKey(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the proyecto with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto, or <code>null</code> if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchByPrimaryKey(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the proyectos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the proyectos where codigo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param codigo the codigo
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the proyectos where codigo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param codigo the codigo
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCodigo(
		java.lang.String codigo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyCodigo_First(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the first proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyCodigo_First(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyCodigo_Last(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the last proyecto in the ordered set where codigo = &#63;.
	*
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyCodigo_Last(
		java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where codigo = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param codigo the codigo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto[] findBybyCodigo_PrevAndNext(
		long idProyecto, java.lang.String codigo,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns all the proyectos where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the proyectos where fechaInicio = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fechaInicio the fecha inicio
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the proyectos where fechaInicio = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fechaInicio the fecha inicio
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyFechaInicio(
		java.util.Date fechaInicio, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyFechaInicio_First(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyFechaInicio_First(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyFechaInicio_Last(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyFechaInicio_Last(
		java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where fechaInicio = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param fechaInicio the fecha inicio
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto[] findBybyFechaInicio_PrevAndNext(
		long idProyecto, java.util.Date fechaInicio,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns all the proyectos where coste = &#63;.
	*
	* @param coste the coste
	* @return the matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the proyectos where coste = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param coste the coste
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the proyectos where coste = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param coste the coste
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findBybyCoste(
		double coste, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyCoste_First(double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the first proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyCoste_First(double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto findBybyCoste_Last(double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns the last proyecto in the ordered set where coste = &#63;.
	*
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto fetchBybyCoste_Last(double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the proyectos before and after the current proyecto in the ordered set where coste = &#63;.
	*
	* @param idProyecto the primary key of the current proyecto
	* @param coste the coste
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next proyecto
	* @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Proyecto[] findBybyCoste_PrevAndNext(
		long idProyecto, double coste,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchProyectoException;

	/**
	* Returns all the proyectos.
	*
	* @return the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Proyecto> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the proyectos where codigo = &#63; from the database.
	*
	* @param codigo the codigo
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyCodigo(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the proyectos where fechaInicio = &#63; from the database.
	*
	* @param fechaInicio the fecha inicio
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyFechaInicio(java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the proyectos where coste = &#63; from the database.
	*
	* @param coste the coste
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyCoste(double coste)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the proyectos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of proyectos where codigo = &#63;.
	*
	* @param codigo the codigo
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyCodigo(java.lang.String codigo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of proyectos where fechaInicio = &#63;.
	*
	* @param fechaInicio the fecha inicio
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyFechaInicio(java.util.Date fechaInicio)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of proyectos where coste = &#63;.
	*
	* @param coste the coste
	* @return the number of matching proyectos
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyCoste(double coste)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of proyectos.
	*
	* @return the number of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tareas associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @return the tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tareas associated with the proyecto.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the proyecto
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getTareas(long pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tareas associated with the proyecto.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the proyecto
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getTareas(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tareas associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @return the number of tareas associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public int getTareasSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the tarea is associated with the proyecto.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @return <code>true</code> if the tarea is associated with the proyecto; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the proyecto has any tareas associated with it.
	*
	* @param pk the primary key of the proyecto to check for associations with tareas
	* @return <code>true</code> if the proyecto has any tareas associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @throws SystemException if a system exception occurred
	*/
	public void addTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tarea the tarea
	* @throws SystemException if a system exception occurred
	*/
	public void addTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas
	* @throws SystemException if a system exception occurred
	*/
	public void addTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas
	* @throws SystemException if a system exception occurred
	*/
	public void addTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Clears all associations between the proyecto and its tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto to clear the associated tareas from
	* @throws SystemException if a system exception occurred
	*/
	public void clearTareas(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPK the primary key of the tarea
	* @throws SystemException if a system exception occurred
	*/
	public void removeTarea(long pk, long tareaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tarea the tarea
	* @throws SystemException if a system exception occurred
	*/
	public void removeTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas
	* @throws SystemException if a system exception occurred
	*/
	public void removeTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas
	* @throws SystemException if a system exception occurred
	*/
	public void removeTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareaPKs the primary keys of the tareas to be associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public void setTareas(long pk, long[] tareaPKs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the proyecto
	* @param tareas the tareas to be associated with the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public void setTareas(long pk,
		java.util.List<com.lrm.gestor.model.Tarea> tareas)
		throws com.liferay.portal.kernel.exception.SystemException;
}