package com.lrm.gestor;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.lrm.gestor.model.Proyecto;
import com.lrm.gestor.model.Tarea;
import com.lrm.gestor.model.impl.ProyectoImpl;
import com.lrm.gestor.service.ProyectoLocalServiceUtil;
import com.lrm.gestor.service.TareaLocalServiceUtil;
import com.lrm.gestor.service.persistence.ProyectoUtil;

/**
 * Portlet implementation class GestorProyectos
 */
public class GestorProyectos extends MVCPortlet {
	
	/**
	 * Da de alta un proyecto nuevo
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @throws IOException
	 * @throws PortletException
	 */
	public void addProject(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String codigo = ParamUtil.getString(actionRequest, "codigo");
		String desc = ParamUtil.getString(actionRequest, "descripcion");
		GregorianCalendar fechaInicio = new GregorianCalendar(
				ParamUtil.getInteger(actionRequest, "fechaInicioYear"),
				ParamUtil.getInteger(actionRequest, "fechaInicioMonth"),
				ParamUtil.getInteger(actionRequest, "fechaInicioDay"));
		
		Double coste = ParamUtil.getDouble(actionRequest, "coste");
		try {
			System.out.println("===addProject===");
			Proyecto proyecto = ProyectoLocalServiceUtil.
					createProyecto(CounterLocalServiceUtil.increment());
			proyecto.setCodigo(codigo);
			proyecto.setDescripcion(desc);
			proyecto.setFechaInicio(fechaInicio.getTime());
			proyecto.setCoste(coste);
			proyecto = ProyectoLocalServiceUtil.addProyecto(proyecto);
			if (proyecto != null) {
				SessionMessages.add(actionRequest.getPortletSession(), "project-add-success");
				System.out.println("project-add-success");
			} else {
				SessionMessages.add(actionRequest.getPortletSession(), "project-add-failed");
				System.out.println("project-add-failed");
			}
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/altaProyecto.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Da de alta una tarea nueva
	 * @param actionRequest
	 * @param actionResponse
	 * @throws IOException
	 * @throws PortletException
	 */
	public void addTask(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String task = ParamUtil.getString(actionRequest, "tarea");
		try {
			System.out.println("===addTask===");
			Tarea tarea = TareaLocalServiceUtil.createTarea(CounterLocalServiceUtil.increment());
			tarea.setTarea(task);
			tarea = TareaLocalServiceUtil.addTarea(tarea);
			if (tarea != null) {
				SessionMessages.add(actionRequest.getPortletSession(), "task-add-success");
				System.out.println("task-add-success");
			} else {
				SessionMessages.add(actionRequest.getPortletSession(), "task-add-failed");
				System.out.println("task-add-failed");
			}
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/altaTarea.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Asigna una tarea a varios proyectos
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @throws IOException
	 * @throws PortletException
	 */
	public void mapProyectos(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		long idTarea = ParamUtil.getLong(actionRequest, "tarea");
		String selectedProjects = ParamUtil.getString(actionRequest, "selectedProyectosHidden");
		long defaultValue = 0;
		long [] idsProyecto = StringUtil.split(selectedProjects, ";", defaultValue);
		try {
			System.out.println("===proyectos===" + idsProyecto.length);
			// Mapeo de muchos proyectos a una tarea
			ProyectoLocalServiceUtil.addTareaProyectos(idTarea, idsProyecto);
			SessionMessages.add(actionRequest.getPortletSession(), "proyectos-add-success");
			System.out.println("proyectos-add-success");
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/map_projects_to_task.jsp");
		} catch (Exception e) {
			SessionMessages.add(actionRequest.getPortletSession(), "proyectos-add-failed");
			System.out.println("proyectos-add-failed");
			e.printStackTrace();
		}
	}
	
	/**
	 * Asigna tareas a un proyecto
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @throws IOException
	 * @throws PortletException
	 */
	public void mapTareas(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		long idProyecto = ParamUtil.getLong(actionRequest, "proyecto");
		String selectedTasks = ParamUtil.getString(actionRequest, "selectedTareasHidden");
		long defaultValue = 0;
		long [] idsTarea = StringUtil.split(selectedTasks, ";", defaultValue);
		try {
			System.out.println("===tareas===" + idsTarea.length);
			// Mapeo de muchas tareas a un proyecto
			TareaLocalServiceUtil.addProyectoTareas(idProyecto, idsTarea);
			SessionMessages.add(actionRequest.getPortletSession(), "tareas-add-success");
			System.out.println("tareas-add-success");
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/map_tasks_to_project.jsp");
		} catch (Exception e) {
			SessionMessages.add(actionRequest.getPortletSession(), "tareas-add-failed");
			System.out.println("tareas-add-failed");
			e.printStackTrace();
		}
	}
	
	/**
	 * Da de baja un proyecto existente
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @throws IOException
	 * @throws PortletException
	 */
	public void deleteProyecto(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		long idProyecto = ParamUtil.getLong(actionRequest, "idProyecto");
		try {
			System.out.println("===deleteProyecto===");
			List<Tarea> tareas = TareaLocalServiceUtil.getProyectoTareas(idProyecto);
			for(int i = 0; i < tareas.size(); i++) {
				TareaLocalServiceUtil.deleteProyectoTarea(idProyecto, tareas.get(i));
			}
			ProyectoLocalServiceUtil.deleteProyecto(idProyecto);
			SessionMessages.add(actionRequest.getPortletSession(), "proyecto-delete-success");
			System.out.println("proyecto-delete-success");
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/display_project_tasks.jsp");
		} catch (Exception e) {
			SessionMessages.add(actionRequest.getPortletSession(), "proyecto-delete-failed");
			System.out.println("proyecto-delete-failed");
			e.printStackTrace();
		}
	}
	
	/**
	 * Lista proyectos por c�digo
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @return lista de proyectos
	 * @throws IOException
	 * @throws PortletException
	 * @throws SystemException
	 */
	public List<Proyecto> getProyectoByCodigo(ActionRequest actionRequest,
			ActionResponse actionResponse) 
					throws IOException, PortletException, SystemException {
		String codigo = ParamUtil.getString(actionRequest, "codigo");
		return ProyectoUtil.findBybyCodigo(codigo);
	}
	
	/**
	 * Modifica un proyecto existente
	 * @param actionRequest acci�n de petici�n
	 * @param actionResponse acci�n de respuesta
	 * @throws IOException
	 * @throws PortletException
	 */
	public void updateProyecto(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		Proyecto proyecto = new ProyectoImpl();
		GregorianCalendar fecha = new GregorianCalendar(
				ParamUtil.getInteger(actionRequest, "fechaInicioYear"),
				ParamUtil.getInteger(actionRequest, "fechaInicioMonth"), 
				ParamUtil.getInteger(actionRequest, "fechaInicioDay"));
		proyecto.setIdProyecto(ParamUtil.getLong(actionRequest, "idProyecto"));
		proyecto.setCodigo(ParamUtil.getString(actionRequest, "codigo"));
		proyecto.setDescripcion(ParamUtil.getString(actionRequest, "descripcion"));
		proyecto.setFechaInicio(fecha.getTime());
		proyecto.setCoste(ParamUtil.getDouble(actionRequest, "coste"));
		try {
			System.out.println("===editProyecto===");
			ProyectoLocalServiceUtil.updateProyecto(proyecto);
			SessionMessages.add(actionRequest.getPortletSession(), "proyecto-edit-success");
			System.out.println("proyecto-edit-success");
			actionResponse.setRenderParameter("mvcPath", "/html/jsp/display_project_tasks.jsp");
		} catch (Exception e) {
			SessionMessages.add(actionRequest.getPortletSession(), "proyecto-edit-failed");
			System.out.println("proyecto-edit-failed");
			e.printStackTrace();
		}
	}
	
}
