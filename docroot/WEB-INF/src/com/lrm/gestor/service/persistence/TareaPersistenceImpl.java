/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.lrm.gestor.NoSuchTareaException;
import com.lrm.gestor.model.Tarea;
import com.lrm.gestor.model.impl.TareaImpl;
import com.lrm.gestor.model.impl.TareaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the tarea service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luis Romero Moreno
 * @see TareaPersistence
 * @see TareaUtil
 * @generated
 */
public class TareaPersistenceImpl extends BasePersistenceImpl<Tarea>
	implements TareaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TareaUtil} to access the tarea persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TareaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED, TareaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED, TareaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the tarea in the entity cache if it is enabled.
	 *
	 * @param tarea the tarea
	 */
	public void cacheResult(Tarea tarea) {
		EntityCacheUtil.putResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaImpl.class, tarea.getPrimaryKey(), tarea);

		tarea.resetOriginalValues();
	}

	/**
	 * Caches the tareas in the entity cache if it is enabled.
	 *
	 * @param tareas the tareas
	 */
	public void cacheResult(List<Tarea> tareas) {
		for (Tarea tarea : tareas) {
			if (EntityCacheUtil.getResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
						TareaImpl.class, tarea.getPrimaryKey()) == null) {
				cacheResult(tarea);
			}
			else {
				tarea.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tareas.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TareaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TareaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tarea.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Tarea tarea) {
		EntityCacheUtil.removeResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaImpl.class, tarea.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Tarea> tareas) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Tarea tarea : tareas) {
			EntityCacheUtil.removeResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
				TareaImpl.class, tarea.getPrimaryKey());
		}
	}

	/**
	 * Creates a new tarea with the primary key. Does not add the tarea to the database.
	 *
	 * @param idTarea the primary key for the new tarea
	 * @return the new tarea
	 */
	public Tarea create(long idTarea) {
		Tarea tarea = new TareaImpl();

		tarea.setNew(true);
		tarea.setPrimaryKey(idTarea);

		return tarea;
	}

	/**
	 * Removes the tarea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idTarea the primary key of the tarea
	 * @return the tarea that was removed
	 * @throws com.lrm.gestor.NoSuchTareaException if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Tarea remove(long idTarea)
		throws NoSuchTareaException, SystemException {
		return remove(Long.valueOf(idTarea));
	}

	/**
	 * Removes the tarea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tarea
	 * @return the tarea that was removed
	 * @throws com.lrm.gestor.NoSuchTareaException if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tarea remove(Serializable primaryKey)
		throws NoSuchTareaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Tarea tarea = (Tarea)session.get(TareaImpl.class, primaryKey);

			if (tarea == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTareaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tarea);
		}
		catch (NoSuchTareaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Tarea removeImpl(Tarea tarea) throws SystemException {
		tarea = toUnwrappedModel(tarea);

		try {
			clearProyectos.clear(tarea.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, tarea);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(tarea);

		return tarea;
	}

	@Override
	public Tarea updateImpl(com.lrm.gestor.model.Tarea tarea, boolean merge)
		throws SystemException {
		tarea = toUnwrappedModel(tarea);

		boolean isNew = tarea.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, tarea, merge);

			tarea.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
			TareaImpl.class, tarea.getPrimaryKey(), tarea);

		return tarea;
	}

	protected Tarea toUnwrappedModel(Tarea tarea) {
		if (tarea instanceof TareaImpl) {
			return tarea;
		}

		TareaImpl tareaImpl = new TareaImpl();

		tareaImpl.setNew(tarea.isNew());
		tareaImpl.setPrimaryKey(tarea.getPrimaryKey());

		tareaImpl.setIdTarea(tarea.getIdTarea());
		tareaImpl.setTarea(tarea.getTarea());

		return tareaImpl;
	}

	/**
	 * Returns the tarea with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tarea
	 * @return the tarea
	 * @throws com.liferay.portal.NoSuchModelException if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tarea findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the tarea with the primary key or throws a {@link com.lrm.gestor.NoSuchTareaException} if it could not be found.
	 *
	 * @param idTarea the primary key of the tarea
	 * @return the tarea
	 * @throws com.lrm.gestor.NoSuchTareaException if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Tarea findByPrimaryKey(long idTarea)
		throws NoSuchTareaException, SystemException {
		Tarea tarea = fetchByPrimaryKey(idTarea);

		if (tarea == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + idTarea);
			}

			throw new NoSuchTareaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				idTarea);
		}

		return tarea;
	}

	/**
	 * Returns the tarea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tarea
	 * @return the tarea, or <code>null</code> if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Tarea fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the tarea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idTarea the primary key of the tarea
	 * @return the tarea, or <code>null</code> if a tarea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Tarea fetchByPrimaryKey(long idTarea) throws SystemException {
		Tarea tarea = (Tarea)EntityCacheUtil.getResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
				TareaImpl.class, idTarea);

		if (tarea == _nullTarea) {
			return null;
		}

		if (tarea == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				tarea = (Tarea)session.get(TareaImpl.class,
						Long.valueOf(idTarea));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (tarea != null) {
					cacheResult(tarea);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(TareaModelImpl.ENTITY_CACHE_ENABLED,
						TareaImpl.class, idTarea, _nullTarea);
				}

				closeSession(session);
			}
		}

		return tarea;
	}

	/**
	 * Returns all the tareas.
	 *
	 * @return the tareas
	 * @throws SystemException if a system exception occurred
	 */
	public List<Tarea> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tareas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of tareas
	 * @param end the upper bound of the range of tareas (not inclusive)
	 * @return the range of tareas
	 * @throws SystemException if a system exception occurred
	 */
	public List<Tarea> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tareas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of tareas
	 * @param end the upper bound of the range of tareas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tareas
	 * @throws SystemException if a system exception occurred
	 */
	public List<Tarea> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Tarea> list = (List<Tarea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TAREA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TAREA.concat(TareaModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Tarea>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Tarea>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tareas from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Tarea tarea : findAll()) {
			remove(tarea);
		}
	}

	/**
	 * Returns the number of tareas.
	 *
	 * @return the number of tareas
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TAREA);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the proyectos associated with the tarea.
	 *
	 * @param pk the primary key of the tarea
	 * @return the proyectos associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Proyecto> getProyectos(long pk)
		throws SystemException {
		return getProyectos(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the proyectos associated with the tarea.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the tarea
	 * @param start the lower bound of the range of tareas
	 * @param end the upper bound of the range of tareas (not inclusive)
	 * @return the range of proyectos associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Proyecto> getProyectos(long pk, int start,
		int end) throws SystemException {
		return getProyectos(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_PROYECTOS = new FinderPath(com.lrm.gestor.model.impl.ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA,
			com.lrm.gestor.model.impl.ProyectoImpl.class,
			TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"getProyectos",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_PROYECTOS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the proyectos associated with the tarea.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the tarea
	 * @param start the lower bound of the range of tareas
	 * @param end the upper bound of the range of tareas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of proyectos associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Proyecto> getProyectos(long pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.lrm.gestor.model.Proyecto> list = (List<com.lrm.gestor.model.Proyecto>)FinderCacheUtil.getResult(FINDER_PATH_GET_PROYECTOS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETPROYECTOS.concat(ORDER_BY_CLAUSE)
										   .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETPROYECTOS.concat(com.lrm.gestor.model.impl.ProyectoModelImpl.ORDER_BY_SQL);
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("LRM_Proyecto",
					com.lrm.gestor.model.impl.ProyectoImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.lrm.gestor.model.Proyecto>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_PROYECTOS,
						finderArgs);
				}
				else {
					proyectoPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_PROYECTOS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_PROYECTOS_SIZE = new FinderPath(com.lrm.gestor.model.impl.ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA, Long.class,
			TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"getProyectosSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_PROYECTOS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of proyectos associated with the tarea.
	 *
	 * @param pk the primary key of the tarea
	 * @return the number of proyectos associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public int getProyectosSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_PROYECTOS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETPROYECTOSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_PROYECTOS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_PROYECTO = new FinderPath(com.lrm.gestor.model.impl.ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			TareaModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA,
			Boolean.class,
			TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"containsProyecto",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the proyecto is associated with the tarea.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPK the primary key of the proyecto
	 * @return <code>true</code> if the proyecto is associated with the tarea; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProyecto(long pk, long proyectoPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, proyectoPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_PROYECTO,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsProyecto.contains(pk, proyectoPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_PROYECTO,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the tarea has any proyectos associated with it.
	 *
	 * @param pk the primary key of the tarea to check for associations with proyectos
	 * @return <code>true</code> if the tarea has any proyectos associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProyectos(long pk) throws SystemException {
		if (getProyectosSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPK the primary key of the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void addProyecto(long pk, long proyectoPK) throws SystemException {
		try {
			addProyecto.add(pk, proyectoPK);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyecto the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void addProyecto(long pk, com.lrm.gestor.model.Proyecto proyecto)
		throws SystemException {
		try {
			addProyecto.add(pk, proyecto.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPKs the primary keys of the proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public void addProyectos(long pk, long[] proyectoPKs)
		throws SystemException {
		try {
			for (long proyectoPK : proyectoPKs) {
				addProyecto.add(pk, proyectoPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectos the proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public void addProyectos(long pk,
		List<com.lrm.gestor.model.Proyecto> proyectos)
		throws SystemException {
		try {
			for (com.lrm.gestor.model.Proyecto proyecto : proyectos) {
				addProyecto.add(pk, proyecto.getPrimaryKey());
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Clears all associations between the tarea and its proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea to clear the associated proyectos from
	 * @throws SystemException if a system exception occurred
	 */
	public void clearProyectos(long pk) throws SystemException {
		try {
			clearProyectos.clear(pk);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPK the primary key of the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void removeProyecto(long pk, long proyectoPK)
		throws SystemException {
		try {
			removeProyecto.remove(pk, proyectoPK);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyecto the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void removeProyecto(long pk, com.lrm.gestor.model.Proyecto proyecto)
		throws SystemException {
		try {
			removeProyecto.remove(pk, proyecto.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPKs the primary keys of the proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public void removeProyectos(long pk, long[] proyectoPKs)
		throws SystemException {
		try {
			for (long proyectoPK : proyectoPKs) {
				removeProyecto.remove(pk, proyectoPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectos the proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public void removeProyectos(long pk,
		List<com.lrm.gestor.model.Proyecto> proyectos)
		throws SystemException {
		try {
			for (com.lrm.gestor.model.Proyecto proyecto : proyectos) {
				removeProyecto.remove(pk, proyecto.getPrimaryKey());
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Sets the proyectos associated with the tarea, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectoPKs the primary keys of the proyectos to be associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void setProyectos(long pk, long[] proyectoPKs)
		throws SystemException {
		try {
			Set<Long> proyectoPKSet = SetUtil.fromArray(proyectoPKs);

			List<com.lrm.gestor.model.Proyecto> proyectos = getProyectos(pk);

			for (com.lrm.gestor.model.Proyecto proyecto : proyectos) {
				if (!proyectoPKSet.remove(proyecto.getPrimaryKey())) {
					removeProyecto.remove(pk, proyecto.getPrimaryKey());
				}
			}

			for (Long proyectoPK : proyectoPKSet) {
				addProyecto.add(pk, proyectoPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Sets the proyectos associated with the tarea, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the tarea
	 * @param proyectos the proyectos to be associated with the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void setProyectos(long pk,
		List<com.lrm.gestor.model.Proyecto> proyectos)
		throws SystemException {
		try {
			long[] proyectoPKs = new long[proyectos.size()];

			for (int i = 0; i < proyectos.size(); i++) {
				com.lrm.gestor.model.Proyecto proyecto = proyectos.get(i);

				proyectoPKs[i] = proyecto.getPrimaryKey();
			}

			setProyectos(pk, proyectoPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(TareaModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Initializes the tarea persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.lrm.gestor.model.Tarea")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Tarea>> listenersList = new ArrayList<ModelListener<Tarea>>();

				for (String listenerClassName : listenerClassNames) {
					Class<?> clazz = getClass();

					listenersList.add((ModelListener<Tarea>)InstanceFactory.newInstance(
							clazz.getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsProyecto = new ContainsProyecto();

		addProyecto = new AddProyecto();
		clearProyectos = new ClearProyectos();
		removeProyecto = new RemoveProyecto();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TareaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ProyectoPersistence.class)
	protected ProyectoPersistence proyectoPersistence;
	@BeanReference(type = TareaPersistence.class)
	protected TareaPersistence tareaPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsProyecto containsProyecto;
	protected AddProyecto addProyecto;
	protected ClearProyectos clearProyectos;
	protected RemoveProyecto removeProyecto;

	protected class ContainsProyecto {
		protected ContainsProyecto() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSPROYECTO,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long idTarea, long idProyecto) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(idTarea), new Long(idProyecto)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class AddProyecto {
		protected AddProyecto() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"INSERT INTO LRM_Proyecto_Tarea (idTarea, idProyecto) VALUES (?, ?)",
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT });
		}

		protected void add(long idTarea, long idProyecto)
			throws SystemException {
			if (!containsProyecto.contains(idTarea, idProyecto)) {
				ModelListener<com.lrm.gestor.model.Proyecto>[] proyectoListeners =
					proyectoPersistence.getListeners();

				for (ModelListener<Tarea> listener : listeners) {
					listener.onBeforeAddAssociation(idTarea,
						com.lrm.gestor.model.Proyecto.class.getName(),
						idProyecto);
				}

				for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
					listener.onBeforeAddAssociation(idProyecto,
						Tarea.class.getName(), idTarea);
				}

				_sqlUpdate.update(new Object[] {
						new Long(idTarea), new Long(idProyecto)
					});

				for (ModelListener<Tarea> listener : listeners) {
					listener.onAfterAddAssociation(idTarea,
						com.lrm.gestor.model.Proyecto.class.getName(),
						idProyecto);
				}

				for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
					listener.onAfterAddAssociation(idProyecto,
						Tarea.class.getName(), idTarea);
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	protected class ClearProyectos {
		protected ClearProyectos() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"DELETE FROM LRM_Proyecto_Tarea WHERE idTarea = ?",
					new int[] { java.sql.Types.BIGINT });
		}

		protected void clear(long idTarea) throws SystemException {
			ModelListener<com.lrm.gestor.model.Proyecto>[] proyectoListeners = proyectoPersistence.getListeners();

			List<com.lrm.gestor.model.Proyecto> proyectos = null;

			if ((listeners.length > 0) || (proyectoListeners.length > 0)) {
				proyectos = getProyectos(idTarea);

				for (com.lrm.gestor.model.Proyecto proyecto : proyectos) {
					for (ModelListener<Tarea> listener : listeners) {
						listener.onBeforeRemoveAssociation(idTarea,
							com.lrm.gestor.model.Proyecto.class.getName(),
							proyecto.getPrimaryKey());
					}

					for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
						listener.onBeforeRemoveAssociation(proyecto.getPrimaryKey(),
							Tarea.class.getName(), idTarea);
					}
				}
			}

			_sqlUpdate.update(new Object[] { new Long(idTarea) });

			if ((listeners.length > 0) || (proyectoListeners.length > 0)) {
				for (com.lrm.gestor.model.Proyecto proyecto : proyectos) {
					for (ModelListener<Tarea> listener : listeners) {
						listener.onAfterRemoveAssociation(idTarea,
							com.lrm.gestor.model.Proyecto.class.getName(),
							proyecto.getPrimaryKey());
					}

					for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
						listener.onAfterRemoveAssociation(proyecto.getPrimaryKey(),
							Tarea.class.getName(), idTarea);
					}
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	protected class RemoveProyecto {
		protected RemoveProyecto() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"DELETE FROM LRM_Proyecto_Tarea WHERE idTarea = ? AND idProyecto = ?",
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT });
		}

		protected void remove(long idTarea, long idProyecto)
			throws SystemException {
			if (containsProyecto.contains(idTarea, idProyecto)) {
				ModelListener<com.lrm.gestor.model.Proyecto>[] proyectoListeners =
					proyectoPersistence.getListeners();

				for (ModelListener<Tarea> listener : listeners) {
					listener.onBeforeRemoveAssociation(idTarea,
						com.lrm.gestor.model.Proyecto.class.getName(),
						idProyecto);
				}

				for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
					listener.onBeforeRemoveAssociation(idProyecto,
						Tarea.class.getName(), idTarea);
				}

				_sqlUpdate.update(new Object[] {
						new Long(idTarea), new Long(idProyecto)
					});

				for (ModelListener<Tarea> listener : listeners) {
					listener.onAfterRemoveAssociation(idTarea,
						com.lrm.gestor.model.Proyecto.class.getName(),
						idProyecto);
				}

				for (ModelListener<com.lrm.gestor.model.Proyecto> listener : proyectoListeners) {
					listener.onAfterRemoveAssociation(idProyecto,
						Tarea.class.getName(), idTarea);
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	private static final String _SQL_SELECT_TAREA = "SELECT tarea FROM Tarea tarea";
	private static final String _SQL_COUNT_TAREA = "SELECT COUNT(tarea) FROM Tarea tarea";
	private static final String _SQL_GETPROYECTOS = "SELECT {LRM_Proyecto.*} FROM LRM_Proyecto INNER JOIN LRM_Proyecto_Tarea ON (LRM_Proyecto_Tarea.idProyecto = LRM_Proyecto.idProyecto) WHERE (LRM_Proyecto_Tarea.idTarea = ?)";
	private static final String _SQL_GETPROYECTOSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM LRM_Proyecto_Tarea WHERE idTarea = ?";
	private static final String _SQL_CONTAINSPROYECTO = "SELECT COUNT(*) AS COUNT_VALUE FROM LRM_Proyecto_Tarea WHERE idTarea = ? AND idProyecto = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tarea.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Tarea exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TareaPersistenceImpl.class);
	private static Tarea _nullTarea = new TareaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Tarea> toCacheModel() {
				return _nullTareaCacheModel;
			}
		};

	private static CacheModel<Tarea> _nullTareaCacheModel = new CacheModel<Tarea>() {
			public Tarea toEntityModel() {
				return _nullTarea;
			}
		};
}