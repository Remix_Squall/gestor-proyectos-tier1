/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link TareaLocalService}.
 * </p>
 *
 * @author    Luis Romero Moreno
 * @see       TareaLocalService
 * @generated
 */
public class TareaLocalServiceWrapper implements TareaLocalService,
	ServiceWrapper<TareaLocalService> {
	public TareaLocalServiceWrapper(TareaLocalService tareaLocalService) {
		_tareaLocalService = tareaLocalService;
	}

	/**
	* Adds the tarea to the database. Also notifies the appropriate model listeners.
	*
	* @param tarea the tarea
	* @return the tarea that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea addTarea(com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.addTarea(tarea);
	}

	/**
	* Creates a new tarea with the primary key. Does not add the tarea to the database.
	*
	* @param idTarea the primary key for the new tarea
	* @return the new tarea
	*/
	public com.lrm.gestor.model.Tarea createTarea(long idTarea) {
		return _tareaLocalService.createTarea(idTarea);
	}

	/**
	* Deletes the tarea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idTarea the primary key of the tarea
	* @return the tarea that was removed
	* @throws PortalException if a tarea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea deleteTarea(long idTarea)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.deleteTarea(idTarea);
	}

	/**
	* Deletes the tarea from the database. Also notifies the appropriate model listeners.
	*
	* @param tarea the tarea
	* @return the tarea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea deleteTarea(
		com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.deleteTarea(tarea);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _tareaLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.lrm.gestor.model.Tarea fetchTarea(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.fetchTarea(idTarea);
	}

	/**
	* Returns the tarea with the primary key.
	*
	* @param idTarea the primary key of the tarea
	* @return the tarea
	* @throws PortalException if a tarea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea getTarea(long idTarea)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getTarea(idTarea);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the tareas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of tareas
	* @param end the upper bound of the range of tareas (not inclusive)
	* @return the range of tareas
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getTareas(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getTareas(start, end);
	}

	/**
	* Returns the number of tareas.
	*
	* @return the number of tareas
	* @throws SystemException if a system exception occurred
	*/
	public int getTareasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getTareasCount();
	}

	/**
	* Updates the tarea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tarea the tarea
	* @return the tarea that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea updateTarea(
		com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.updateTarea(tarea);
	}

	/**
	* Updates the tarea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param tarea the tarea
	* @param merge whether to merge the tarea with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the tarea that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.lrm.gestor.model.Tarea updateTarea(
		com.lrm.gestor.model.Tarea tarea, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.updateTarea(tarea, merge);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addProyectoTarea(long idProyecto, long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.addProyectoTarea(idProyecto, idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addProyectoTarea(long idProyecto,
		com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.addProyectoTarea(idProyecto, tarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addProyectoTareas(long idProyecto, long[] idTareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.addProyectoTareas(idProyecto, idTareas);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void addProyectoTareas(long idProyecto,
		java.util.List<com.lrm.gestor.model.Tarea> Tareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.addProyectoTareas(idProyecto, Tareas);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void clearProyectoTareas(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.clearProyectoTareas(idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteProyectoTarea(long idProyecto, long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.deleteProyectoTarea(idProyecto, idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteProyectoTarea(long idProyecto,
		com.lrm.gestor.model.Tarea tarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.deleteProyectoTarea(idProyecto, tarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteProyectoTareas(long idProyecto, long[] idTareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.deleteProyectoTareas(idProyecto, idTareas);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void deleteProyectoTareas(long idProyecto,
		java.util.List<com.lrm.gestor.model.Tarea> Tareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.deleteProyectoTareas(idProyecto, Tareas);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getProyectoTareas(
		long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getProyectoTareas(idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getProyectoTareas(
		long idProyecto, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getProyectoTareas(idProyecto, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.lrm.gestor.model.Tarea> getProyectoTareas(
		long idProyecto, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getProyectoTareas(idProyecto, start, end,
			orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public int getProyectoTareasCount(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.getProyectoTareasCount(idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public boolean hasProyectoTarea(long idProyecto, long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.hasProyectoTarea(idProyecto, idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public boolean hasProyectoTareas(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _tareaLocalService.hasProyectoTareas(idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public void setProyectoTareas(long idProyecto, long[] idTareas)
		throws com.liferay.portal.kernel.exception.SystemException {
		_tareaLocalService.setProyectoTareas(idProyecto, idTareas);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _tareaLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_tareaLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _tareaLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public TareaLocalService getWrappedTareaLocalService() {
		return _tareaLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedTareaLocalService(TareaLocalService tareaLocalService) {
		_tareaLocalService = tareaLocalService;
	}

	public TareaLocalService getWrappedService() {
		return _tareaLocalService;
	}

	public void setWrappedService(TareaLocalService tareaLocalService) {
		_tareaLocalService = tareaLocalService;
	}

	private TareaLocalService _tareaLocalService;
}