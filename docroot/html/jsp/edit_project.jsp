<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.lrm.gestor.model.Proyecto"%>
<%@page import="com.lrm.gestor.service.ProyectoLocalServiceUtil"%>
<%@page import="com.lrm.gestor.model.Tarea"%>
<%@page import="java.util.List"%>
<%@page import="com.lrm.gestor.service.TareaLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>

<%
	long idProyecto = ParamUtil.getLong(request, "idProyecto");
	Proyecto proyecto = ProyectoLocalServiceUtil
	.getProyecto(idProyecto);
%>
<portlet:actionURL name="updateProyecto" var="updateURL" />
<portlet:renderURL var="displayURL">
	<portlet:param name="mvcPath"
		value="/html/jsp/display_project_tasks.jsp" />
</portlet:renderURL>
<aui:model-context bean="<%=proyecto%>" model="<%=Proyecto.class%>" />
<aui:form action="<%=updateURL%>">
	<aui:fieldset>
		<aui:input type="hidden" name="idProyecto"
			value="<%=String.valueOf(proyecto.getIdProyecto())%>" />
		<aui:input type="hidden" name="codigo"
			value="<%=String.valueOf(proyecto.getCodigo())%>" />
		<p><b>C�digo de proyecto:</b> <%=proyecto.getCodigo() %></p>
		<aui:input name="descripcion" label="Descripci�n proyecto" />
		<aui:input name="fechaInicio" label="Fecha Inicio" />
<aui:input name="coste" label="Coste" />
	</aui:fieldset>
	<aui:button-row>
		<aui:button type="submit" />
		<aui:button type="cancel" onClick="<%=displayURL %>" />
	</aui:button-row>
</aui:form>