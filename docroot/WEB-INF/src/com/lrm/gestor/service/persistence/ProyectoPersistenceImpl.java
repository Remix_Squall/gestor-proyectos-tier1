/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.lrm.gestor.NoSuchProyectoException;
import com.lrm.gestor.model.Proyecto;
import com.lrm.gestor.model.impl.ProyectoImpl;
import com.lrm.gestor.model.impl.ProyectoModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the proyecto service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luis Romero Moreno
 * @see ProyectoPersistence
 * @see ProyectoUtil
 * @generated
 */
public class ProyectoPersistenceImpl extends BasePersistenceImpl<Proyecto>
	implements ProyectoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProyectoUtil} to access the proyecto persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProyectoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCODIGO = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybyCodigo",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCODIGO =
		new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyCodigo",
			new String[] { String.class.getName() },
			ProyectoModelImpl.CODIGO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYCODIGO = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyCodigo",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYFECHAINICIO =
		new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybyFechaInicio",
			new String[] {
				Date.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYFECHAINICIO =
		new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyFechaInicio",
			new String[] { Date.class.getName() },
			ProyectoModelImpl.FECHAINICIO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYFECHAINICIO = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyFechaInicio",
			new String[] { Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCOSTE = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybyCoste",
			new String[] {
				Double.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCOSTE =
		new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyCoste",
			new String[] { Double.class.getName() },
			ProyectoModelImpl.COSTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYCOSTE = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyCoste",
			new String[] { Double.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, ProyectoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the proyecto in the entity cache if it is enabled.
	 *
	 * @param proyecto the proyecto
	 */
	public void cacheResult(Proyecto proyecto) {
		EntityCacheUtil.putResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoImpl.class, proyecto.getPrimaryKey(), proyecto);

		proyecto.resetOriginalValues();
	}

	/**
	 * Caches the proyectos in the entity cache if it is enabled.
	 *
	 * @param proyectos the proyectos
	 */
	public void cacheResult(List<Proyecto> proyectos) {
		for (Proyecto proyecto : proyectos) {
			if (EntityCacheUtil.getResult(
						ProyectoModelImpl.ENTITY_CACHE_ENABLED,
						ProyectoImpl.class, proyecto.getPrimaryKey()) == null) {
				cacheResult(proyecto);
			}
			else {
				proyecto.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all proyectos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProyectoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProyectoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the proyecto.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Proyecto proyecto) {
		EntityCacheUtil.removeResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoImpl.class, proyecto.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Proyecto> proyectos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Proyecto proyecto : proyectos) {
			EntityCacheUtil.removeResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
				ProyectoImpl.class, proyecto.getPrimaryKey());
		}
	}

	/**
	 * Creates a new proyecto with the primary key. Does not add the proyecto to the database.
	 *
	 * @param idProyecto the primary key for the new proyecto
	 * @return the new proyecto
	 */
	public Proyecto create(long idProyecto) {
		Proyecto proyecto = new ProyectoImpl();

		proyecto.setNew(true);
		proyecto.setPrimaryKey(idProyecto);

		return proyecto;
	}

	/**
	 * Removes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idProyecto the primary key of the proyecto
	 * @return the proyecto that was removed
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto remove(long idProyecto)
		throws NoSuchProyectoException, SystemException {
		return remove(Long.valueOf(idProyecto));
	}

	/**
	 * Removes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the proyecto
	 * @return the proyecto that was removed
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Proyecto remove(Serializable primaryKey)
		throws NoSuchProyectoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Proyecto proyecto = (Proyecto)session.get(ProyectoImpl.class,
					primaryKey);

			if (proyecto == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProyectoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(proyecto);
		}
		catch (NoSuchProyectoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Proyecto removeImpl(Proyecto proyecto) throws SystemException {
		proyecto = toUnwrappedModel(proyecto);

		try {
			clearTareas.clear(proyecto.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, proyecto);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(proyecto);

		return proyecto;
	}

	@Override
	public Proyecto updateImpl(com.lrm.gestor.model.Proyecto proyecto,
		boolean merge) throws SystemException {
		proyecto = toUnwrappedModel(proyecto);

		boolean isNew = proyecto.isNew();

		ProyectoModelImpl proyectoModelImpl = (ProyectoModelImpl)proyecto;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, proyecto, merge);

			proyecto.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProyectoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((proyectoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCODIGO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						proyectoModelImpl.getOriginalCodigo()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCODIGO, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCODIGO,
					args);

				args = new Object[] { proyectoModelImpl.getCodigo() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCODIGO, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCODIGO,
					args);
			}

			if ((proyectoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYFECHAINICIO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						proyectoModelImpl.getOriginalFechaInicio()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYFECHAINICIO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYFECHAINICIO,
					args);

				args = new Object[] { proyectoModelImpl.getFechaInicio() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYFECHAINICIO,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYFECHAINICIO,
					args);
			}

			if ((proyectoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCOSTE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Double.valueOf(proyectoModelImpl.getOriginalCoste())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCOSTE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCOSTE,
					args);

				args = new Object[] { Double.valueOf(proyectoModelImpl.getCoste()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCOSTE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCOSTE,
					args);
			}
		}

		EntityCacheUtil.putResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoImpl.class, proyecto.getPrimaryKey(), proyecto);

		return proyecto;
	}

	protected Proyecto toUnwrappedModel(Proyecto proyecto) {
		if (proyecto instanceof ProyectoImpl) {
			return proyecto;
		}

		ProyectoImpl proyectoImpl = new ProyectoImpl();

		proyectoImpl.setNew(proyecto.isNew());
		proyectoImpl.setPrimaryKey(proyecto.getPrimaryKey());

		proyectoImpl.setIdProyecto(proyecto.getIdProyecto());
		proyectoImpl.setCodigo(proyecto.getCodigo());
		proyectoImpl.setDescripcion(proyecto.getDescripcion());
		proyectoImpl.setFechaInicio(proyecto.getFechaInicio());
		proyectoImpl.setCoste(proyecto.getCoste());

		return proyectoImpl;
	}

	/**
	 * Returns the proyecto with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the proyecto
	 * @return the proyecto
	 * @throws com.liferay.portal.NoSuchModelException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Proyecto findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the proyecto with the primary key or throws a {@link com.lrm.gestor.NoSuchProyectoException} if it could not be found.
	 *
	 * @param idProyecto the primary key of the proyecto
	 * @return the proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findByPrimaryKey(long idProyecto)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchByPrimaryKey(idProyecto);

		if (proyecto == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + idProyecto);
			}

			throw new NoSuchProyectoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				idProyecto);
		}

		return proyecto;
	}

	/**
	 * Returns the proyecto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the proyecto
	 * @return the proyecto, or <code>null</code> if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Proyecto fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the proyecto with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idProyecto the primary key of the proyecto
	 * @return the proyecto, or <code>null</code> if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchByPrimaryKey(long idProyecto)
		throws SystemException {
		Proyecto proyecto = (Proyecto)EntityCacheUtil.getResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
				ProyectoImpl.class, idProyecto);

		if (proyecto == _nullProyecto) {
			return null;
		}

		if (proyecto == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				proyecto = (Proyecto)session.get(ProyectoImpl.class,
						Long.valueOf(idProyecto));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (proyecto != null) {
					cacheResult(proyecto);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ProyectoModelImpl.ENTITY_CACHE_ENABLED,
						ProyectoImpl.class, idProyecto, _nullProyecto);
				}

				closeSession(session);
			}
		}

		return proyecto;
	}

	/**
	 * Returns all the proyectos where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @return the matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCodigo(String codigo)
		throws SystemException {
		return findBybyCodigo(codigo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the proyectos where codigo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param codigo the codigo
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @return the range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCodigo(String codigo, int start, int end)
		throws SystemException {
		return findBybyCodigo(codigo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the proyectos where codigo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param codigo the codigo
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCodigo(String codigo, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCODIGO;
			finderArgs = new Object[] { codigo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCODIGO;
			finderArgs = new Object[] { codigo, start, end, orderByComparator };
		}

		List<Proyecto> list = (List<Proyecto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Proyecto proyecto : list) {
				if (!Validator.equals(codigo, proyecto.getCodigo())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROYECTO_WHERE);

			if (codigo == null) {
				query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_1);
			}
			else {
				if (codigo.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_3);
				}
				else {
					query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(ProyectoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (codigo != null) {
					qPos.add(codigo);
				}

				list = (List<Proyecto>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first proyecto in the ordered set where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyCodigo_First(String codigo,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyCodigo_First(codigo, orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigo=");
		msg.append(codigo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the first proyecto in the ordered set where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyCodigo_First(String codigo,
		OrderByComparator orderByComparator) throws SystemException {
		List<Proyecto> list = findBybyCodigo(codigo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last proyecto in the ordered set where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyCodigo_Last(String codigo,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyCodigo_Last(codigo, orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("codigo=");
		msg.append(codigo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the last proyecto in the ordered set where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyCodigo_Last(String codigo,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyCodigo(codigo);

		List<Proyecto> list = findBybyCodigo(codigo, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the proyectos before and after the current proyecto in the ordered set where codigo = &#63;.
	 *
	 * @param idProyecto the primary key of the current proyecto
	 * @param codigo the codigo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto[] findBybyCodigo_PrevAndNext(long idProyecto,
		String codigo, OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = findByPrimaryKey(idProyecto);

		Session session = null;

		try {
			session = openSession();

			Proyecto[] array = new ProyectoImpl[3];

			array[0] = getBybyCodigo_PrevAndNext(session, proyecto, codigo,
					orderByComparator, true);

			array[1] = proyecto;

			array[2] = getBybyCodigo_PrevAndNext(session, proyecto, codigo,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Proyecto getBybyCodigo_PrevAndNext(Session session,
		Proyecto proyecto, String codigo, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROYECTO_WHERE);

		if (codigo == null) {
			query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_1);
		}
		else {
			if (codigo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_3);
			}
			else {
				query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(ProyectoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (codigo != null) {
			qPos.add(codigo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(proyecto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Proyecto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the proyectos where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @return the matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyFechaInicio(Date fechaInicio)
		throws SystemException {
		return findBybyFechaInicio(fechaInicio, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the proyectos where fechaInicio = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param fechaInicio the fecha inicio
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @return the range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyFechaInicio(Date fechaInicio, int start,
		int end) throws SystemException {
		return findBybyFechaInicio(fechaInicio, start, end, null);
	}

	/**
	 * Returns an ordered range of all the proyectos where fechaInicio = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param fechaInicio the fecha inicio
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyFechaInicio(Date fechaInicio, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYFECHAINICIO;
			finderArgs = new Object[] { fechaInicio };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYFECHAINICIO;
			finderArgs = new Object[] { fechaInicio, start, end, orderByComparator };
		}

		List<Proyecto> list = (List<Proyecto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Proyecto proyecto : list) {
				if (!Validator.equals(fechaInicio, proyecto.getFechaInicio())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROYECTO_WHERE);

			if (fechaInicio == null) {
				query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_1);
			}
			else {
				query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(ProyectoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (fechaInicio != null) {
					qPos.add(CalendarUtil.getTimestamp(fechaInicio));
				}

				list = (List<Proyecto>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyFechaInicio_First(Date fechaInicio,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyFechaInicio_First(fechaInicio,
				orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fechaInicio=");
		msg.append(fechaInicio);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the first proyecto in the ordered set where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyFechaInicio_First(Date fechaInicio,
		OrderByComparator orderByComparator) throws SystemException {
		List<Proyecto> list = findBybyFechaInicio(fechaInicio, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyFechaInicio_Last(Date fechaInicio,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyFechaInicio_Last(fechaInicio,
				orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fechaInicio=");
		msg.append(fechaInicio);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the last proyecto in the ordered set where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyFechaInicio_Last(Date fechaInicio,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyFechaInicio(fechaInicio);

		List<Proyecto> list = findBybyFechaInicio(fechaInicio, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the proyectos before and after the current proyecto in the ordered set where fechaInicio = &#63;.
	 *
	 * @param idProyecto the primary key of the current proyecto
	 * @param fechaInicio the fecha inicio
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto[] findBybyFechaInicio_PrevAndNext(long idProyecto,
		Date fechaInicio, OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = findByPrimaryKey(idProyecto);

		Session session = null;

		try {
			session = openSession();

			Proyecto[] array = new ProyectoImpl[3];

			array[0] = getBybyFechaInicio_PrevAndNext(session, proyecto,
					fechaInicio, orderByComparator, true);

			array[1] = proyecto;

			array[2] = getBybyFechaInicio_PrevAndNext(session, proyecto,
					fechaInicio, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Proyecto getBybyFechaInicio_PrevAndNext(Session session,
		Proyecto proyecto, Date fechaInicio,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROYECTO_WHERE);

		if (fechaInicio == null) {
			query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_1);
		}
		else {
			query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(ProyectoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (fechaInicio != null) {
			qPos.add(CalendarUtil.getTimestamp(fechaInicio));
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(proyecto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Proyecto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the proyectos where coste = &#63;.
	 *
	 * @param coste the coste
	 * @return the matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCoste(double coste) throws SystemException {
		return findBybyCoste(coste, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the proyectos where coste = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param coste the coste
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @return the range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCoste(double coste, int start, int end)
		throws SystemException {
		return findBybyCoste(coste, start, end, null);
	}

	/**
	 * Returns an ordered range of all the proyectos where coste = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param coste the coste
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findBybyCoste(double coste, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCOSTE;
			finderArgs = new Object[] { coste };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCOSTE;
			finderArgs = new Object[] { coste, start, end, orderByComparator };
		}

		List<Proyecto> list = (List<Proyecto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Proyecto proyecto : list) {
				if ((coste != proyecto.getCoste())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROYECTO_WHERE);

			query.append(_FINDER_COLUMN_BYCOSTE_COSTE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(ProyectoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(coste);

				list = (List<Proyecto>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first proyecto in the ordered set where coste = &#63;.
	 *
	 * @param coste the coste
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyCoste_First(double coste,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyCoste_First(coste, orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("coste=");
		msg.append(coste);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the first proyecto in the ordered set where coste = &#63;.
	 *
	 * @param coste the coste
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyCoste_First(double coste,
		OrderByComparator orderByComparator) throws SystemException {
		List<Proyecto> list = findBybyCoste(coste, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last proyecto in the ordered set where coste = &#63;.
	 *
	 * @param coste the coste
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto findBybyCoste_Last(double coste,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = fetchBybyCoste_Last(coste, orderByComparator);

		if (proyecto != null) {
			return proyecto;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("coste=");
		msg.append(coste);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProyectoException(msg.toString());
	}

	/**
	 * Returns the last proyecto in the ordered set where coste = &#63;.
	 *
	 * @param coste the coste
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching proyecto, or <code>null</code> if a matching proyecto could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto fetchBybyCoste_Last(double coste,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyCoste(coste);

		List<Proyecto> list = findBybyCoste(coste, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the proyectos before and after the current proyecto in the ordered set where coste = &#63;.
	 *
	 * @param idProyecto the primary key of the current proyecto
	 * @param coste the coste
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next proyecto
	 * @throws com.lrm.gestor.NoSuchProyectoException if a proyecto with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Proyecto[] findBybyCoste_PrevAndNext(long idProyecto, double coste,
		OrderByComparator orderByComparator)
		throws NoSuchProyectoException, SystemException {
		Proyecto proyecto = findByPrimaryKey(idProyecto);

		Session session = null;

		try {
			session = openSession();

			Proyecto[] array = new ProyectoImpl[3];

			array[0] = getBybyCoste_PrevAndNext(session, proyecto, coste,
					orderByComparator, true);

			array[1] = proyecto;

			array[2] = getBybyCoste_PrevAndNext(session, proyecto, coste,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Proyecto getBybyCoste_PrevAndNext(Session session,
		Proyecto proyecto, double coste, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROYECTO_WHERE);

		query.append(_FINDER_COLUMN_BYCOSTE_COSTE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(ProyectoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(coste);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(proyecto);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Proyecto> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the proyectos.
	 *
	 * @return the proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the proyectos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @return the range of proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the proyectos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Proyecto> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Proyecto> list = (List<Proyecto>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PROYECTO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROYECTO.concat(ProyectoModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Proyecto>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Proyecto>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the proyectos where codigo = &#63; from the database.
	 *
	 * @param codigo the codigo
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybyCodigo(String codigo) throws SystemException {
		for (Proyecto proyecto : findBybyCodigo(codigo)) {
			remove(proyecto);
		}
	}

	/**
	 * Removes all the proyectos where fechaInicio = &#63; from the database.
	 *
	 * @param fechaInicio the fecha inicio
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybyFechaInicio(Date fechaInicio)
		throws SystemException {
		for (Proyecto proyecto : findBybyFechaInicio(fechaInicio)) {
			remove(proyecto);
		}
	}

	/**
	 * Removes all the proyectos where coste = &#63; from the database.
	 *
	 * @param coste the coste
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybyCoste(double coste) throws SystemException {
		for (Proyecto proyecto : findBybyCoste(coste)) {
			remove(proyecto);
		}
	}

	/**
	 * Removes all the proyectos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Proyecto proyecto : findAll()) {
			remove(proyecto);
		}
	}

	/**
	 * Returns the number of proyectos where codigo = &#63;.
	 *
	 * @param codigo the codigo
	 * @return the number of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybyCodigo(String codigo) throws SystemException {
		Object[] finderArgs = new Object[] { codigo };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BYCODIGO,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROYECTO_WHERE);

			if (codigo == null) {
				query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_1);
			}
			else {
				if (codigo.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_3);
				}
				else {
					query.append(_FINDER_COLUMN_BYCODIGO_CODIGO_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (codigo != null) {
					qPos.add(codigo);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BYCODIGO,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of proyectos where fechaInicio = &#63;.
	 *
	 * @param fechaInicio the fecha inicio
	 * @return the number of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybyFechaInicio(Date fechaInicio) throws SystemException {
		Object[] finderArgs = new Object[] { fechaInicio };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BYFECHAINICIO,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROYECTO_WHERE);

			if (fechaInicio == null) {
				query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_1);
			}
			else {
				query.append(_FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (fechaInicio != null) {
					qPos.add(CalendarUtil.getTimestamp(fechaInicio));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BYFECHAINICIO,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of proyectos where coste = &#63;.
	 *
	 * @param coste the coste
	 * @return the number of matching proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybyCoste(double coste) throws SystemException {
		Object[] finderArgs = new Object[] { coste };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BYCOSTE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROYECTO_WHERE);

			query.append(_FINDER_COLUMN_BYCOSTE_COSTE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(coste);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BYCOSTE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of proyectos.
	 *
	 * @return the number of proyectos
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROYECTO);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the tareas associated with the proyecto.
	 *
	 * @param pk the primary key of the proyecto
	 * @return the tareas associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Tarea> getTareas(long pk)
		throws SystemException {
		return getTareas(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the tareas associated with the proyecto.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the proyecto
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @return the range of tareas associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Tarea> getTareas(long pk, int start,
		int end) throws SystemException {
		return getTareas(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_TAREAS = new FinderPath(com.lrm.gestor.model.impl.TareaModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA,
			com.lrm.gestor.model.impl.TareaImpl.class,
			ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"getTareas",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_TAREAS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the tareas associated with the proyecto.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the proyecto
	 * @param start the lower bound of the range of proyectos
	 * @param end the upper bound of the range of proyectos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tareas associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.lrm.gestor.model.Tarea> getTareas(long pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.lrm.gestor.model.Tarea> list = (List<com.lrm.gestor.model.Tarea>)FinderCacheUtil.getResult(FINDER_PATH_GET_TAREAS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETTAREAS.concat(ORDER_BY_CLAUSE)
										.concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETTAREAS.concat(com.lrm.gestor.model.impl.TareaModelImpl.ORDER_BY_SQL);
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("LRM_Tarea",
					com.lrm.gestor.model.impl.TareaImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.lrm.gestor.model.Tarea>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_TAREAS,
						finderArgs);
				}
				else {
					tareaPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_TAREAS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_TAREAS_SIZE = new FinderPath(com.lrm.gestor.model.impl.TareaModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA,
			Long.class,
			ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"getTareasSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_TAREAS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of tareas associated with the proyecto.
	 *
	 * @param pk the primary key of the proyecto
	 * @return the number of tareas associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public int getTareasSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_TAREAS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETTAREASSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_TAREAS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_TAREA = new FinderPath(com.lrm.gestor.model.impl.TareaModelImpl.ENTITY_CACHE_ENABLED,
			ProyectoModelImpl.FINDER_CACHE_ENABLED_LRM_PROYECTO_TAREA,
			Boolean.class,
			ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME,
			"containsTarea",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the tarea is associated with the proyecto.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPK the primary key of the tarea
	 * @return <code>true</code> if the tarea is associated with the proyecto; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsTarea(long pk, long tareaPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, tareaPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_TAREA,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsTarea.contains(pk, tareaPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_TAREA,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the proyecto has any tareas associated with it.
	 *
	 * @param pk the primary key of the proyecto to check for associations with tareas
	 * @return <code>true</code> if the proyecto has any tareas associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsTareas(long pk) throws SystemException {
		if (getTareasSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPK the primary key of the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void addTarea(long pk, long tareaPK) throws SystemException {
		try {
			addTarea.add(pk, tareaPK);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tarea the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void addTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws SystemException {
		try {
			addTarea.add(pk, tarea.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPKs the primary keys of the tareas
	 * @throws SystemException if a system exception occurred
	 */
	public void addTareas(long pk, long[] tareaPKs) throws SystemException {
		try {
			for (long tareaPK : tareaPKs) {
				addTarea.add(pk, tareaPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Adds an association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareas the tareas
	 * @throws SystemException if a system exception occurred
	 */
	public void addTareas(long pk, List<com.lrm.gestor.model.Tarea> tareas)
		throws SystemException {
		try {
			for (com.lrm.gestor.model.Tarea tarea : tareas) {
				addTarea.add(pk, tarea.getPrimaryKey());
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Clears all associations between the proyecto and its tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto to clear the associated tareas from
	 * @throws SystemException if a system exception occurred
	 */
	public void clearTareas(long pk) throws SystemException {
		try {
			clearTareas.clear(pk);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPK the primary key of the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void removeTarea(long pk, long tareaPK) throws SystemException {
		try {
			removeTarea.remove(pk, tareaPK);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the proyecto and the tarea. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tarea the tarea
	 * @throws SystemException if a system exception occurred
	 */
	public void removeTarea(long pk, com.lrm.gestor.model.Tarea tarea)
		throws SystemException {
		try {
			removeTarea.remove(pk, tarea.getPrimaryKey());
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPKs the primary keys of the tareas
	 * @throws SystemException if a system exception occurred
	 */
	public void removeTareas(long pk, long[] tareaPKs)
		throws SystemException {
		try {
			for (long tareaPK : tareaPKs) {
				removeTarea.remove(pk, tareaPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Removes the association between the proyecto and the tareas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareas the tareas
	 * @throws SystemException if a system exception occurred
	 */
	public void removeTareas(long pk, List<com.lrm.gestor.model.Tarea> tareas)
		throws SystemException {
		try {
			for (com.lrm.gestor.model.Tarea tarea : tareas) {
				removeTarea.remove(pk, tarea.getPrimaryKey());
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareaPKs the primary keys of the tareas to be associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void setTareas(long pk, long[] tareaPKs) throws SystemException {
		try {
			Set<Long> tareaPKSet = SetUtil.fromArray(tareaPKs);

			List<com.lrm.gestor.model.Tarea> tareas = getTareas(pk);

			for (com.lrm.gestor.model.Tarea tarea : tareas) {
				if (!tareaPKSet.remove(tarea.getPrimaryKey())) {
					removeTarea.remove(pk, tarea.getPrimaryKey());
				}
			}

			for (Long tareaPK : tareaPKSet) {
				addTarea.add(pk, tareaPK);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Sets the tareas associated with the proyecto, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the proyecto
	 * @param tareas the tareas to be associated with the proyecto
	 * @throws SystemException if a system exception occurred
	 */
	public void setTareas(long pk, List<com.lrm.gestor.model.Tarea> tareas)
		throws SystemException {
		try {
			long[] tareaPKs = new long[tareas.size()];

			for (int i = 0; i < tareas.size(); i++) {
				com.lrm.gestor.model.Tarea tarea = tareas.get(i);

				tareaPKs[i] = tarea.getPrimaryKey();
			}

			setTareas(pk, tareaPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			FinderCacheUtil.clearCache(ProyectoModelImpl.MAPPING_TABLE_LRM_PROYECTO_TAREA_NAME);
		}
	}

	/**
	 * Initializes the proyecto persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.lrm.gestor.model.Proyecto")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Proyecto>> listenersList = new ArrayList<ModelListener<Proyecto>>();

				for (String listenerClassName : listenerClassNames) {
					Class<?> clazz = getClass();

					listenersList.add((ModelListener<Proyecto>)InstanceFactory.newInstance(
							clazz.getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsTarea = new ContainsTarea();

		addTarea = new AddTarea();
		clearTareas = new ClearTareas();
		removeTarea = new RemoveTarea();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProyectoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ProyectoPersistence.class)
	protected ProyectoPersistence proyectoPersistence;
	@BeanReference(type = TareaPersistence.class)
	protected TareaPersistence tareaPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsTarea containsTarea;
	protected AddTarea addTarea;
	protected ClearTareas clearTareas;
	protected RemoveTarea removeTarea;

	protected class ContainsTarea {
		protected ContainsTarea() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSTAREA,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long idProyecto, long idTarea) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(idProyecto), new Long(idTarea)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class AddTarea {
		protected AddTarea() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"INSERT INTO LRM_Proyecto_Tarea (idProyecto, idTarea) VALUES (?, ?)",
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT });
		}

		protected void add(long idProyecto, long idTarea)
			throws SystemException {
			if (!containsTarea.contains(idProyecto, idTarea)) {
				ModelListener<com.lrm.gestor.model.Tarea>[] tareaListeners = tareaPersistence.getListeners();

				for (ModelListener<Proyecto> listener : listeners) {
					listener.onBeforeAddAssociation(idProyecto,
						com.lrm.gestor.model.Tarea.class.getName(), idTarea);
				}

				for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
					listener.onBeforeAddAssociation(idTarea,
						Proyecto.class.getName(), idProyecto);
				}

				_sqlUpdate.update(new Object[] {
						new Long(idProyecto), new Long(idTarea)
					});

				for (ModelListener<Proyecto> listener : listeners) {
					listener.onAfterAddAssociation(idProyecto,
						com.lrm.gestor.model.Tarea.class.getName(), idTarea);
				}

				for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
					listener.onAfterAddAssociation(idTarea,
						Proyecto.class.getName(), idProyecto);
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	protected class ClearTareas {
		protected ClearTareas() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"DELETE FROM LRM_Proyecto_Tarea WHERE idProyecto = ?",
					new int[] { java.sql.Types.BIGINT });
		}

		protected void clear(long idProyecto) throws SystemException {
			ModelListener<com.lrm.gestor.model.Tarea>[] tareaListeners = tareaPersistence.getListeners();

			List<com.lrm.gestor.model.Tarea> tareas = null;

			if ((listeners.length > 0) || (tareaListeners.length > 0)) {
				tareas = getTareas(idProyecto);

				for (com.lrm.gestor.model.Tarea tarea : tareas) {
					for (ModelListener<Proyecto> listener : listeners) {
						listener.onBeforeRemoveAssociation(idProyecto,
							com.lrm.gestor.model.Tarea.class.getName(),
							tarea.getPrimaryKey());
					}

					for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
						listener.onBeforeRemoveAssociation(tarea.getPrimaryKey(),
							Proyecto.class.getName(), idProyecto);
					}
				}
			}

			_sqlUpdate.update(new Object[] { new Long(idProyecto) });

			if ((listeners.length > 0) || (tareaListeners.length > 0)) {
				for (com.lrm.gestor.model.Tarea tarea : tareas) {
					for (ModelListener<Proyecto> listener : listeners) {
						listener.onAfterRemoveAssociation(idProyecto,
							com.lrm.gestor.model.Tarea.class.getName(),
							tarea.getPrimaryKey());
					}

					for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
						listener.onAfterRemoveAssociation(tarea.getPrimaryKey(),
							Proyecto.class.getName(), idProyecto);
					}
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	protected class RemoveTarea {
		protected RemoveTarea() {
			_sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(getDataSource(),
					"DELETE FROM LRM_Proyecto_Tarea WHERE idProyecto = ? AND idTarea = ?",
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT });
		}

		protected void remove(long idProyecto, long idTarea)
			throws SystemException {
			if (containsTarea.contains(idProyecto, idTarea)) {
				ModelListener<com.lrm.gestor.model.Tarea>[] tareaListeners = tareaPersistence.getListeners();

				for (ModelListener<Proyecto> listener : listeners) {
					listener.onBeforeRemoveAssociation(idProyecto,
						com.lrm.gestor.model.Tarea.class.getName(), idTarea);
				}

				for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
					listener.onBeforeRemoveAssociation(idTarea,
						Proyecto.class.getName(), idProyecto);
				}

				_sqlUpdate.update(new Object[] {
						new Long(idProyecto), new Long(idTarea)
					});

				for (ModelListener<Proyecto> listener : listeners) {
					listener.onAfterRemoveAssociation(idProyecto,
						com.lrm.gestor.model.Tarea.class.getName(), idTarea);
				}

				for (ModelListener<com.lrm.gestor.model.Tarea> listener : tareaListeners) {
					listener.onAfterRemoveAssociation(idTarea,
						Proyecto.class.getName(), idProyecto);
				}
			}
		}

		private SqlUpdate _sqlUpdate;
	}

	private static final String _SQL_SELECT_PROYECTO = "SELECT proyecto FROM Proyecto proyecto";
	private static final String _SQL_SELECT_PROYECTO_WHERE = "SELECT proyecto FROM Proyecto proyecto WHERE ";
	private static final String _SQL_COUNT_PROYECTO = "SELECT COUNT(proyecto) FROM Proyecto proyecto";
	private static final String _SQL_COUNT_PROYECTO_WHERE = "SELECT COUNT(proyecto) FROM Proyecto proyecto WHERE ";
	private static final String _SQL_GETTAREAS = "SELECT {LRM_Tarea.*} FROM LRM_Tarea INNER JOIN LRM_Proyecto_Tarea ON (LRM_Proyecto_Tarea.idTarea = LRM_Tarea.idTarea) WHERE (LRM_Proyecto_Tarea.idProyecto = ?)";
	private static final String _SQL_GETTAREASSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM LRM_Proyecto_Tarea WHERE idProyecto = ?";
	private static final String _SQL_CONTAINSTAREA = "SELECT COUNT(*) AS COUNT_VALUE FROM LRM_Proyecto_Tarea WHERE idProyecto = ? AND idTarea = ?";
	private static final String _FINDER_COLUMN_BYCODIGO_CODIGO_1 = "proyecto.codigo IS NULL";
	private static final String _FINDER_COLUMN_BYCODIGO_CODIGO_2 = "proyecto.codigo = ?";
	private static final String _FINDER_COLUMN_BYCODIGO_CODIGO_3 = "(proyecto.codigo IS NULL OR proyecto.codigo = ?)";
	private static final String _FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_1 = "proyecto.fechaInicio IS NULL";
	private static final String _FINDER_COLUMN_BYFECHAINICIO_FECHAINICIO_2 = "proyecto.fechaInicio = ?";
	private static final String _FINDER_COLUMN_BYCOSTE_COSTE_2 = "proyecto.coste = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "proyecto.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Proyecto exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Proyecto exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProyectoPersistenceImpl.class);
	private static Proyecto _nullProyecto = new ProyectoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Proyecto> toCacheModel() {
				return _nullProyectoCacheModel;
			}
		};

	private static CacheModel<Proyecto> _nullProyectoCacheModel = new CacheModel<Proyecto>() {
			public Proyecto toEntityModel() {
				return _nullProyecto;
			}
		};
}