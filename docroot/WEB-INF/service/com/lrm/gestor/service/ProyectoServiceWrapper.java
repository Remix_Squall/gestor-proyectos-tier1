/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ProyectoService}.
 * </p>
 *
 * @author    Luis Romero Moreno
 * @see       ProyectoService
 * @generated
 */
public class ProyectoServiceWrapper implements ProyectoService,
	ServiceWrapper<ProyectoService> {
	public ProyectoServiceWrapper(ProyectoService proyectoService) {
		_proyectoService = proyectoService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _proyectoService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_proyectoService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _proyectoService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ProyectoService getWrappedProyectoService() {
		return _proyectoService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedProyectoService(ProyectoService proyectoService) {
		_proyectoService = proyectoService;
	}

	public ProyectoService getWrappedService() {
		return _proyectoService;
	}

	public void setWrappedService(ProyectoService proyectoService) {
		_proyectoService = proyectoService;
	}

	private ProyectoService _proyectoService;
}