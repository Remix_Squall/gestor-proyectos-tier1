/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.lrm.gestor.model.Tarea;

import java.io.Serializable;

/**
 * The cache model class for representing Tarea in entity cache.
 *
 * @author Luis Romero Moreno
 * @see Tarea
 * @generated
 */
public class TareaCacheModel implements CacheModel<Tarea>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{idTarea=");
		sb.append(idTarea);
		sb.append(", tarea=");
		sb.append(tarea);
		sb.append("}");

		return sb.toString();
	}

	public Tarea toEntityModel() {
		TareaImpl tareaImpl = new TareaImpl();

		tareaImpl.setIdTarea(idTarea);

		if (tarea == null) {
			tareaImpl.setTarea(StringPool.BLANK);
		}
		else {
			tareaImpl.setTarea(tarea);
		}

		tareaImpl.resetOriginalValues();

		return tareaImpl;
	}

	public long idTarea;
	public String tarea;
}