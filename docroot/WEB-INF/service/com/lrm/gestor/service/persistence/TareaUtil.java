/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.lrm.gestor.model.Tarea;

import java.util.List;

/**
 * The persistence utility for the tarea service. This utility wraps {@link TareaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Luis Romero Moreno
 * @see TareaPersistence
 * @see TareaPersistenceImpl
 * @generated
 */
public class TareaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Tarea tarea) {
		getPersistence().clearCache(tarea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Tarea> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Tarea> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Tarea> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Tarea update(Tarea tarea, boolean merge)
		throws SystemException {
		return getPersistence().update(tarea, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Tarea update(Tarea tarea, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(tarea, merge, serviceContext);
	}

	/**
	* Caches the tarea in the entity cache if it is enabled.
	*
	* @param tarea the tarea
	*/
	public static void cacheResult(com.lrm.gestor.model.Tarea tarea) {
		getPersistence().cacheResult(tarea);
	}

	/**
	* Caches the tareas in the entity cache if it is enabled.
	*
	* @param tareas the tareas
	*/
	public static void cacheResult(
		java.util.List<com.lrm.gestor.model.Tarea> tareas) {
		getPersistence().cacheResult(tareas);
	}

	/**
	* Creates a new tarea with the primary key. Does not add the tarea to the database.
	*
	* @param idTarea the primary key for the new tarea
	* @return the new tarea
	*/
	public static com.lrm.gestor.model.Tarea create(long idTarea) {
		return getPersistence().create(idTarea);
	}

	/**
	* Removes the tarea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idTarea the primary key of the tarea
	* @return the tarea that was removed
	* @throws com.lrm.gestor.NoSuchTareaException if a tarea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Tarea remove(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchTareaException {
		return getPersistence().remove(idTarea);
	}

	public static com.lrm.gestor.model.Tarea updateImpl(
		com.lrm.gestor.model.Tarea tarea, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(tarea, merge);
	}

	/**
	* Returns the tarea with the primary key or throws a {@link com.lrm.gestor.NoSuchTareaException} if it could not be found.
	*
	* @param idTarea the primary key of the tarea
	* @return the tarea
	* @throws com.lrm.gestor.NoSuchTareaException if a tarea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Tarea findByPrimaryKey(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.lrm.gestor.NoSuchTareaException {
		return getPersistence().findByPrimaryKey(idTarea);
	}

	/**
	* Returns the tarea with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idTarea the primary key of the tarea
	* @return the tarea, or <code>null</code> if a tarea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Tarea fetchByPrimaryKey(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(idTarea);
	}

	/**
	* Returns all the tareas.
	*
	* @return the tareas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the tareas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of tareas
	* @param end the upper bound of the range of tareas (not inclusive)
	* @return the range of tareas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the tareas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of tareas
	* @param end the upper bound of the range of tareas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tareas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Tarea> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the tareas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of tareas.
	*
	* @return the number of tareas
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the proyectos associated with the tarea.
	*
	* @param pk the primary key of the tarea
	* @return the proyectos associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getProyectos(
		long pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProyectos(pk);
	}

	/**
	* Returns a range of all the proyectos associated with the tarea.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the tarea
	* @param start the lower bound of the range of tareas
	* @param end the upper bound of the range of tareas (not inclusive)
	* @return the range of proyectos associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getProyectos(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProyectos(pk, start, end);
	}

	/**
	* Returns an ordered range of all the proyectos associated with the tarea.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the tarea
	* @param start the lower bound of the range of tareas
	* @param end the upper bound of the range of tareas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of proyectos associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getProyectos(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProyectos(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of proyectos associated with the tarea.
	*
	* @param pk the primary key of the tarea
	* @return the number of proyectos associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static int getProyectosSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProyectosSize(pk);
	}

	/**
	* Returns <code>true</code> if the proyecto is associated with the tarea.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPK the primary key of the proyecto
	* @return <code>true</code> if the proyecto is associated with the tarea; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProyecto(long pk, long proyectoPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProyecto(pk, proyectoPK);
	}

	/**
	* Returns <code>true</code> if the tarea has any proyectos associated with it.
	*
	* @param pk the primary key of the tarea to check for associations with proyectos
	* @return <code>true</code> if the tarea has any proyectos associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProyectos(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProyectos(pk);
	}

	/**
	* Adds an association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPK the primary key of the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void addProyecto(long pk, long proyectoPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addProyecto(pk, proyectoPK);
	}

	/**
	* Adds an association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyecto the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void addProyecto(long pk,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addProyecto(pk, proyecto);
	}

	/**
	* Adds an association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPKs the primary keys of the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static void addProyectos(long pk, long[] proyectoPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addProyectos(pk, proyectoPKs);
	}

	/**
	* Adds an association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectos the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static void addProyectos(long pk,
		java.util.List<com.lrm.gestor.model.Proyecto> proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().addProyectos(pk, proyectos);
	}

	/**
	* Clears all associations between the tarea and its proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea to clear the associated proyectos from
	* @throws SystemException if a system exception occurred
	*/
	public static void clearProyectos(long pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().clearProyectos(pk);
	}

	/**
	* Removes the association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPK the primary key of the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void removeProyecto(long pk, long proyectoPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeProyecto(pk, proyectoPK);
	}

	/**
	* Removes the association between the tarea and the proyecto. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyecto the proyecto
	* @throws SystemException if a system exception occurred
	*/
	public static void removeProyecto(long pk,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeProyecto(pk, proyecto);
	}

	/**
	* Removes the association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPKs the primary keys of the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static void removeProyectos(long pk, long[] proyectoPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeProyectos(pk, proyectoPKs);
	}

	/**
	* Removes the association between the tarea and the proyectos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectos the proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static void removeProyectos(long pk,
		java.util.List<com.lrm.gestor.model.Proyecto> proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeProyectos(pk, proyectos);
	}

	/**
	* Sets the proyectos associated with the tarea, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectoPKs the primary keys of the proyectos to be associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void setProyectos(long pk, long[] proyectoPKs)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setProyectos(pk, proyectoPKs);
	}

	/**
	* Sets the proyectos associated with the tarea, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the tarea
	* @param proyectos the proyectos to be associated with the tarea
	* @throws SystemException if a system exception occurred
	*/
	public static void setProyectos(long pk,
		java.util.List<com.lrm.gestor.model.Proyecto> proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().setProyectos(pk, proyectos);
	}

	public static TareaPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (TareaPersistence)PortletBeanLocatorUtil.locate(com.lrm.gestor.service.ClpSerializer.getServletContextName(),
					TareaPersistence.class.getName());

			ReferenceRegistry.registerReference(TareaUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(TareaPersistence persistence) {
	}

	private static TareaPersistence _persistence;
}