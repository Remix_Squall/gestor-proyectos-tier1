/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Proyecto}.
 * </p>
 *
 * @author    Luis Romero Moreno
 * @see       Proyecto
 * @generated
 */
public class ProyectoWrapper implements Proyecto, ModelWrapper<Proyecto> {
	public ProyectoWrapper(Proyecto proyecto) {
		_proyecto = proyecto;
	}

	public Class<?> getModelClass() {
		return Proyecto.class;
	}

	public String getModelClassName() {
		return Proyecto.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idProyecto", getIdProyecto());
		attributes.put("codigo", getCodigo());
		attributes.put("descripcion", getDescripcion());
		attributes.put("fechaInicio", getFechaInicio());
		attributes.put("coste", getCoste());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long idProyecto = (Long)attributes.get("idProyecto");

		if (idProyecto != null) {
			setIdProyecto(idProyecto);
		}

		String codigo = (String)attributes.get("codigo");

		if (codigo != null) {
			setCodigo(codigo);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		Date fechaInicio = (Date)attributes.get("fechaInicio");

		if (fechaInicio != null) {
			setFechaInicio(fechaInicio);
		}

		Double coste = (Double)attributes.get("coste");

		if (coste != null) {
			setCoste(coste);
		}
	}

	/**
	* Returns the primary key of this proyecto.
	*
	* @return the primary key of this proyecto
	*/
	public long getPrimaryKey() {
		return _proyecto.getPrimaryKey();
	}

	/**
	* Sets the primary key of this proyecto.
	*
	* @param primaryKey the primary key of this proyecto
	*/
	public void setPrimaryKey(long primaryKey) {
		_proyecto.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id proyecto of this proyecto.
	*
	* @return the id proyecto of this proyecto
	*/
	public long getIdProyecto() {
		return _proyecto.getIdProyecto();
	}

	/**
	* Sets the id proyecto of this proyecto.
	*
	* @param idProyecto the id proyecto of this proyecto
	*/
	public void setIdProyecto(long idProyecto) {
		_proyecto.setIdProyecto(idProyecto);
	}

	/**
	* Returns the codigo of this proyecto.
	*
	* @return the codigo of this proyecto
	*/
	public java.lang.String getCodigo() {
		return _proyecto.getCodigo();
	}

	/**
	* Sets the codigo of this proyecto.
	*
	* @param codigo the codigo of this proyecto
	*/
	public void setCodigo(java.lang.String codigo) {
		_proyecto.setCodigo(codigo);
	}

	/**
	* Returns the descripcion of this proyecto.
	*
	* @return the descripcion of this proyecto
	*/
	public java.lang.String getDescripcion() {
		return _proyecto.getDescripcion();
	}

	/**
	* Sets the descripcion of this proyecto.
	*
	* @param descripcion the descripcion of this proyecto
	*/
	public void setDescripcion(java.lang.String descripcion) {
		_proyecto.setDescripcion(descripcion);
	}

	/**
	* Returns the fecha inicio of this proyecto.
	*
	* @return the fecha inicio of this proyecto
	*/
	public java.util.Date getFechaInicio() {
		return _proyecto.getFechaInicio();
	}

	/**
	* Sets the fecha inicio of this proyecto.
	*
	* @param fechaInicio the fecha inicio of this proyecto
	*/
	public void setFechaInicio(java.util.Date fechaInicio) {
		_proyecto.setFechaInicio(fechaInicio);
	}

	/**
	* Returns the coste of this proyecto.
	*
	* @return the coste of this proyecto
	*/
	public double getCoste() {
		return _proyecto.getCoste();
	}

	/**
	* Sets the coste of this proyecto.
	*
	* @param coste the coste of this proyecto
	*/
	public void setCoste(double coste) {
		_proyecto.setCoste(coste);
	}

	public boolean isNew() {
		return _proyecto.isNew();
	}

	public void setNew(boolean n) {
		_proyecto.setNew(n);
	}

	public boolean isCachedModel() {
		return _proyecto.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_proyecto.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _proyecto.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _proyecto.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_proyecto.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _proyecto.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_proyecto.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProyectoWrapper((Proyecto)_proyecto.clone());
	}

	public int compareTo(Proyecto proyecto) {
		return _proyecto.compareTo(proyecto);
	}

	@Override
	public int hashCode() {
		return _proyecto.hashCode();
	}

	public com.liferay.portal.model.CacheModel<Proyecto> toCacheModel() {
		return _proyecto.toCacheModel();
	}

	public Proyecto toEscapedModel() {
		return new ProyectoWrapper(_proyecto.toEscapedModel());
	}

	public Proyecto toUnescapedModel() {
		return new ProyectoWrapper(_proyecto.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _proyecto.toString();
	}

	public java.lang.String toXmlString() {
		return _proyecto.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_proyecto.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProyectoWrapper)) {
			return false;
		}

		ProyectoWrapper proyectoWrapper = (ProyectoWrapper)obj;

		if (Validator.equals(_proyecto, proyectoWrapper._proyecto)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Proyecto getWrappedProyecto() {
		return _proyecto;
	}

	public Proyecto getWrappedModel() {
		return _proyecto;
	}

	public void resetOriginalValues() {
		_proyecto.resetOriginalValues();
	}

	private Proyecto _proyecto;
}