create table LRM_Proyecto (
	idProyecto LONG not null primary key,
	codigo VARCHAR(75) null,
	descripcion TEXT null,
	fechaInicio DATE null,
	coste DOUBLE
);

create table LRM_Proyecto_Tarea (
	idProyecto LONG not null,
	idTarea LONG not null,
	primary key (idProyecto, idTarea)
);

create table LRM_Tarea (
	idTarea LONG not null primary key,
	tarea VARCHAR(75) null
);