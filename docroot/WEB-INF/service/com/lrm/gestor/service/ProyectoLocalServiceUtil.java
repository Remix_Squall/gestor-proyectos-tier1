/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the proyecto local service. This utility wraps {@link com.lrm.gestor.service.impl.ProyectoLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Luis Romero Moreno
 * @see ProyectoLocalService
 * @see com.lrm.gestor.service.base.ProyectoLocalServiceBaseImpl
 * @see com.lrm.gestor.service.impl.ProyectoLocalServiceImpl
 * @generated
 */
public class ProyectoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.lrm.gestor.service.impl.ProyectoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the proyecto to the database. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto addProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addProyecto(proyecto);
	}

	/**
	* Creates a new proyecto with the primary key. Does not add the proyecto to the database.
	*
	* @param idProyecto the primary key for the new proyecto
	* @return the new proyecto
	*/
	public static com.lrm.gestor.model.Proyecto createProyecto(long idProyecto) {
		return getService().createProyecto(idProyecto);
	}

	/**
	* Deletes the proyecto with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto that was removed
	* @throws PortalException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto deleteProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteProyecto(idProyecto);
	}

	/**
	* Deletes the proyecto from the database. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto deleteProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteProyecto(proyecto);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.lrm.gestor.model.Proyecto fetchProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchProyecto(idProyecto);
	}

	/**
	* Returns the proyecto with the primary key.
	*
	* @param idProyecto the primary key of the proyecto
	* @return the proyecto
	* @throws PortalException if a proyecto with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto getProyecto(long idProyecto)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getProyecto(idProyecto);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the proyectos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of proyectos
	* @param end the upper bound of the range of proyectos (not inclusive)
	* @return the range of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getProyectos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getProyectos(start, end);
	}

	/**
	* Returns the number of proyectos.
	*
	* @return the number of proyectos
	* @throws SystemException if a system exception occurred
	*/
	public static int getProyectosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getProyectosCount();
	}

	/**
	* Updates the proyecto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @return the proyecto that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto updateProyecto(
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateProyecto(proyecto);
	}

	/**
	* Updates the proyecto in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param proyecto the proyecto
	* @param merge whether to merge the proyecto with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the proyecto that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.lrm.gestor.model.Proyecto updateProyecto(
		com.lrm.gestor.model.Proyecto proyecto, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateProyecto(proyecto, merge);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareaProyecto(long idTarea,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addTareaProyecto(idTarea, proyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addTareaProyectos(idTarea, idProyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void addTareaProyectos(long idTarea,
		java.util.List<com.lrm.gestor.model.Proyecto> Proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().addTareaProyectos(idTarea, Proyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void clearTareaProyectos(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().clearTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteTareaProyecto(long idTarea,
		com.lrm.gestor.model.Proyecto proyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteTareaProyecto(idTarea, proyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteTareaProyectos(idTarea, idProyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteTareaProyectos(long idTarea,
		java.util.List<com.lrm.gestor.model.Proyecto> Proyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteTareaProyectos(idTarea, Proyectos);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTareaProyectos(idTarea, start, end);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.lrm.gestor.model.Proyecto> getTareaProyectos(
		long idTarea, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getTareaProyectos(idTarea, start, end, orderByComparator);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static int getTareaProyectosCount(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTareaProyectosCount(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasTareaProyecto(long idTarea, long idProyecto)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasTareaProyecto(idTarea, idProyecto);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static boolean hasTareaProyectos(long idTarea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().hasTareaProyectos(idTarea);
	}

	/**
	* @throws SystemException if a system exception occurred
	*/
	public static void setTareaProyectos(long idTarea, long[] idProyectos)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().setTareaProyectos(idTarea, idProyectos);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static ProyectoLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ProyectoLocalService.class.getName());

			if (invokableLocalService instanceof ProyectoLocalService) {
				_service = (ProyectoLocalService)invokableLocalService;
			}
			else {
				_service = new ProyectoLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ProyectoLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(ProyectoLocalService service) {
	}

	private static ProyectoLocalService _service;
}