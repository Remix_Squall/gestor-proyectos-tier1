/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.lrm.gestor.service.http.TareaServiceSoap}.
 *
 * @author    Luis Romero Moreno
 * @see       com.lrm.gestor.service.http.TareaServiceSoap
 * @generated
 */
public class TareaSoap implements Serializable {
	public static TareaSoap toSoapModel(Tarea model) {
		TareaSoap soapModel = new TareaSoap();

		soapModel.setIdTarea(model.getIdTarea());
		soapModel.setTarea(model.getTarea());

		return soapModel;
	}

	public static TareaSoap[] toSoapModels(Tarea[] models) {
		TareaSoap[] soapModels = new TareaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TareaSoap[][] toSoapModels(Tarea[][] models) {
		TareaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TareaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TareaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TareaSoap[] toSoapModels(List<Tarea> models) {
		List<TareaSoap> soapModels = new ArrayList<TareaSoap>(models.size());

		for (Tarea model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TareaSoap[soapModels.size()]);
	}

	public TareaSoap() {
	}

	public long getPrimaryKey() {
		return _idTarea;
	}

	public void setPrimaryKey(long pk) {
		setIdTarea(pk);
	}

	public long getIdTarea() {
		return _idTarea;
	}

	public void setIdTarea(long idTarea) {
		_idTarea = idTarea;
	}

	public String getTarea() {
		return _tarea;
	}

	public void setTarea(String tarea) {
		_tarea = tarea;
	}

	private long _idTarea;
	private String _tarea;
}