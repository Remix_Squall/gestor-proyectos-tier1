/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Tarea}.
 * </p>
 *
 * @author    Luis Romero Moreno
 * @see       Tarea
 * @generated
 */
public class TareaWrapper implements Tarea, ModelWrapper<Tarea> {
	public TareaWrapper(Tarea tarea) {
		_tarea = tarea;
	}

	public Class<?> getModelClass() {
		return Tarea.class;
	}

	public String getModelClassName() {
		return Tarea.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idTarea", getIdTarea());
		attributes.put("tarea", getTarea());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long idTarea = (Long)attributes.get("idTarea");

		if (idTarea != null) {
			setIdTarea(idTarea);
		}

		String tarea = (String)attributes.get("tarea");

		if (tarea != null) {
			setTarea(tarea);
		}
	}

	/**
	* Returns the primary key of this tarea.
	*
	* @return the primary key of this tarea
	*/
	public long getPrimaryKey() {
		return _tarea.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tarea.
	*
	* @param primaryKey the primary key of this tarea
	*/
	public void setPrimaryKey(long primaryKey) {
		_tarea.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the id tarea of this tarea.
	*
	* @return the id tarea of this tarea
	*/
	public long getIdTarea() {
		return _tarea.getIdTarea();
	}

	/**
	* Sets the id tarea of this tarea.
	*
	* @param idTarea the id tarea of this tarea
	*/
	public void setIdTarea(long idTarea) {
		_tarea.setIdTarea(idTarea);
	}

	/**
	* Returns the tarea of this tarea.
	*
	* @return the tarea of this tarea
	*/
	public java.lang.String getTarea() {
		return _tarea.getTarea();
	}

	/**
	* Sets the tarea of this tarea.
	*
	* @param tarea the tarea of this tarea
	*/
	public void setTarea(java.lang.String tarea) {
		_tarea.setTarea(tarea);
	}

	public boolean isNew() {
		return _tarea.isNew();
	}

	public void setNew(boolean n) {
		_tarea.setNew(n);
	}

	public boolean isCachedModel() {
		return _tarea.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_tarea.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _tarea.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _tarea.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_tarea.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _tarea.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_tarea.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TareaWrapper((Tarea)_tarea.clone());
	}

	public int compareTo(Tarea tarea) {
		return _tarea.compareTo(tarea);
	}

	@Override
	public int hashCode() {
		return _tarea.hashCode();
	}

	public com.liferay.portal.model.CacheModel<Tarea> toCacheModel() {
		return _tarea.toCacheModel();
	}

	public Tarea toEscapedModel() {
		return new TareaWrapper(_tarea.toEscapedModel());
	}

	public Tarea toUnescapedModel() {
		return new TareaWrapper(_tarea.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _tarea.toString();
	}

	public java.lang.String toXmlString() {
		return _tarea.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_tarea.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TareaWrapper)) {
			return false;
		}

		TareaWrapper tareaWrapper = (TareaWrapper)obj;

		if (Validator.equals(_tarea, tareaWrapper._tarea)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Tarea getWrappedTarea() {
		return _tarea;
	}

	public Tarea getWrappedModel() {
		return _tarea;
	}

	public void resetOriginalValues() {
		_tarea.resetOriginalValues();
	}

	private Tarea _tarea;
}