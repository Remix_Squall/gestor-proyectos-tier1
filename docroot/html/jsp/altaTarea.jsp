<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ include file="init.jsp"%>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(),
						"task-add-success")%>'>
	<liferay-ui:success key="task-add-success"
		message="La tarea ha sido a�adida con �xito." />
</c:if>
<c:if
	test='<%=SessionMessages.contains(
						renderRequest.getPortletSession(),
						"task-add-failed")%>'>
	<liferay-ui:error key="task-add-failed"
		message="Fallo al a�adir la tarea." />
</c:if>
<portlet:actionURL var="addTaskActionURL" name="addTask">
</portlet:actionURL>
<h1>Add Course</h1>
<aui:form action="<%=addTaskActionURL%>" method="post" name="fm">
	<aui:input name="tarea" value="" label="Tarea" />
	<aui:button-row>
		<aui:button value="Alta tarea" type="submit" />
	</aui:button-row>
</aui:form>