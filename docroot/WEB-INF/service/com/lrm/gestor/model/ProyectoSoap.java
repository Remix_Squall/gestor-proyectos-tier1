/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.lrm.gestor.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.lrm.gestor.service.http.ProyectoServiceSoap}.
 *
 * @author    Luis Romero Moreno
 * @see       com.lrm.gestor.service.http.ProyectoServiceSoap
 * @generated
 */
public class ProyectoSoap implements Serializable {
	public static ProyectoSoap toSoapModel(Proyecto model) {
		ProyectoSoap soapModel = new ProyectoSoap();

		soapModel.setIdProyecto(model.getIdProyecto());
		soapModel.setCodigo(model.getCodigo());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setFechaInicio(model.getFechaInicio());
		soapModel.setCoste(model.getCoste());

		return soapModel;
	}

	public static ProyectoSoap[] toSoapModels(Proyecto[] models) {
		ProyectoSoap[] soapModels = new ProyectoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProyectoSoap[][] toSoapModels(Proyecto[][] models) {
		ProyectoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProyectoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProyectoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProyectoSoap[] toSoapModels(List<Proyecto> models) {
		List<ProyectoSoap> soapModels = new ArrayList<ProyectoSoap>(models.size());

		for (Proyecto model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProyectoSoap[soapModels.size()]);
	}

	public ProyectoSoap() {
	}

	public long getPrimaryKey() {
		return _idProyecto;
	}

	public void setPrimaryKey(long pk) {
		setIdProyecto(pk);
	}

	public long getIdProyecto() {
		return _idProyecto;
	}

	public void setIdProyecto(long idProyecto) {
		_idProyecto = idProyecto;
	}

	public String getCodigo() {
		return _codigo;
	}

	public void setCodigo(String codigo) {
		_codigo = codigo;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public Date getFechaInicio() {
		return _fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		_fechaInicio = fechaInicio;
	}

	public double getCoste() {
		return _coste;
	}

	public void setCoste(double coste) {
		_coste = coste;
	}

	private long _idProyecto;
	private String _codigo;
	private String _descripcion;
	private Date _fechaInicio;
	private double _coste;
}